import platform

if platform.system() == 'Linux':
    filepath = '/usr/src/telephonist/'
else:
    filepath = '/Users/gogasca/Documents/OpenSource/Development/Python/telephonist/'

# =========================================================
# Provider
# =========================================================

valid_providers = ["twilio", "sip"]
default_provider = "twilio"  # Default SIP Provider

# =========================================================
# Campaigns
# =========================================================

country_prefix = {'+1': '1', '+52': '1'}  # We use 1 for Mexico as is required for Cellphones
extra_delay = False
extra_delay_value = 100  # MS
hangup_reason_file = filepath + '/conf/reasons.yaml'
invalid_numbers = filepath + '/log/invalid_numbers.err'
max_calls_per_campaign = 100000
max_cps = 10
max_call_duration = 120
max_campaigns = 5
provider_folder = 'conf/providers/'
provider_test = True
sip_blacklist_enabled = True
sip_provider = "freeswitch"
sip_from = {"+1": ["+16097266805"], "+52": ["+523341600832", "+528141605573", "+525541618824"]}
sip_callerid = "+16097266805"
sip_call_duration = 18
sip_hangup_reason = 'ALLOTTED_TIMEOUT'  # https://wiki.freeswitch.org/wiki/Hangup_Causes
sip_fax_params = "fax_verbose=true,fax_enable_t38=true,fax_use_ecm=false,fax_enable_t38_request=false"


# =========================================================
# API
# =========================================================

api_logfile = filepath + '/log/apid.log'
api_frontend = "loadbalancer"  # Change to HAProxy Address
api_recording_host = "recording"
api_external_port = 8080  # Change to HAProxy external port
api_external_recording_port = 8081
api_scheme = "http"
api_url = api_scheme + "://" + api_frontend + ":" + str(api_external_port) + "/api/1.0"
api_recording_url = api_scheme + "://" + api_recording_host + ":" + str(api_external_recording_port) + "/api/1.0"
api_call_request = api_url + '/call'
api_record_request = api_recording_url + '/record'
api_version = "0.1"
api_base_url = "/api/1.0/"
api_account = "AC64861838b417b555d1c8868705e4453f"
api_password = "YYPKpbIAYqz90oMN8A11YYPKpbIAYqz90o"
api_ip_address = "0.0.0.0"
api_port = 8081  # Defined in apid.sh:8081 for gunicorn telephonist_api:api_app --bind 0.0.0.0:8081
api_client_max_requests = 250
api_error_limit = 5000
api_error_client = filepath + '/log/api_client.err'
api_error_recording_file = filepath + '/log/api_record.err'
api_error_speech_r = filepath + '/log/api_speech_r.err'
items_per_page = 10

# =========================================================
# Call Control
# =========================================================

cps = 1
call_status_check = 2
call_duration = 18
call_country_code = "+1"
call_block_duplicates = True
download_error_file = filepath + '/log/download_record.err'
loop_delay = 120
record_enable = True
record_download = False
record_processing = True
record_duration = 18
record_silence_duration = 5
record_beep = False
record_local_directory = "/var/log/telephonist/recordings/"
record_directory = "/var/www/html/recordings/"
record_languages = {'1': 'en-US', '52': 'es-MX', '55': 'pt_BR'}
record_scheme = "http"
record_server_port = "8081"

# =========================================================
# Speech recognition
# =========================================================

audio_dir = '/var/log/telephonist/fs-recordings/'
# if field is blank then it is left to autodetect to determine language
# so only leave blank if the languages corresponding to the area code are
# very different like english and french for country code 1
language_codes = {'1':'','52':'es','55':'pt-br'}
phone_label_begin='_'
phone_label_end='_'
speech_recognition = True
speech_recognition_language = "en-US"  # "pt_BR"
text_analysis = False
max_distance = 5

# =========================================================
# Lookup enable
# =========================================================

lookup_enable = False
lookup_mexico = filepath + 'conf/providers/lookup/mexico.csv'

# =========================================================
# Twilio configuration parameters
# =========================================================

sms_alerts = True
twilio_from = "+14088053951"
twilio_url = api_url + "/callrecord"
twilio_statuscallback = api_url + "/callstatus"
twilio_call_duration = 20
twilio_file = filepath + 'conf/twilio.conf'
twilio_accountId = 'ACed0b00c221c2e58a3f6dd33308c84321'
twilio_tokenId = '68314e5765c801ba4e38bdb4c730f90e'
phone_numbers = ["+14082186575", "+14084641502"]

# =========================================================
# Database
# =========================================================
# psql -h 198.199.110.24 -d telephonistdb -U telephonist -W #dbhost = '104.236.173.109'
dbhost = 'cluster1-db1-sp1-aws.czcjx1n3is2u.sa-east-1.rds.amazonaws.com'
dbport = 5432
dbusername = 'telephonist'
dbpassword = 'telephonist'
dbname = 'telephonistdb_dev'
dbPasswordAllowEmptyPassword = False

# =========================================================
# SQLALCHEMY parameter
# =========================================================

# "postgresql://telephonist:telephonist@198.199.110.24/telephonistdb
SQLALCHEMY_DATABASE_URI = "postgresql://" + dbusername + ":" + dbpassword + "@" + dbhost + "/" + dbname
SQLALCHEMY_DSN = 'dbname=' + dbname + \
                 ' host=' + dbhost + \
                 ' port=' + str(dbport) + \
                 ' user=' + dbusername + \
                 ' password=' + dbpassword
SQLALCHEMY_ECHO = False
SQLALCHEMY_POOL_SIZE = 1024
SQLALCHEMY_POOL_TIMEOUT = 5
SQLALCHEMY_MAX_OVERFLOW = 0
SQLALCHEMY_POOL_RECYCLE = 60
SQLALCHEMY_TRACK_MODIFICATIONS = True
DATABASE_CONNECT_OPTIONS = None
SQLALCHEMY_COMMIT_ON_TEARDOWN = True

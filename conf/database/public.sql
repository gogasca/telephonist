/*
 Navicat PostgreSQL Data Transfer

 Source Server         : cluster1-db1.sp1.aws
 Source Server Version : 90502
 Source Host           : cluster1-db1-sp1-aws.czcjx1n3is2u.sa-east-1.rds.amazonaws.com
 Source Database       : telephonistdb_dev
 Source Schema         : public

 Target Server Version : 90502
 File Encoding         : utf-8

 Date: 01/01/2017 18:40:42 PM
*/

-- ----------------------------
--  Sequence structure for api_user_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."api_user_id_seq";
CREATE SEQUENCE "public"."api_user_id_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."api_user_id_seq" OWNER TO "telephonist";

-- ----------------------------
--  Sequence structure for calls_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."calls_id_seq";
CREATE SEQUENCE "public"."calls_id_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."calls_id_seq" OWNER TO "telephonist";

-- ----------------------------
--  Sequence structure for campaign_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."campaign_id_seq";
CREATE SEQUENCE "public"."campaign_id_seq" INCREMENT 1 START 1 MAXVALUE 9223372036854775807 MINVALUE 1 CACHE 1;
ALTER TABLE "public"."campaign_id_seq" OWNER TO "telephonist";

-- ----------------------------
--  Table structure for api_user
-- ----------------------------
DROP TABLE IF EXISTS "public"."api_user";
CREATE TABLE "public"."api_user" (
	"id" int4 NOT NULL DEFAULT nextval('api_user_id_seq'::regclass),
	"username" varchar(50) COLLATE "default",
	"password" varchar(250) COLLATE "default",
	"created" timestamp(6) WITH TIME ZONE
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."api_user" OWNER TO "telephonist";

-- ----------------------------
--  Table structure for campaign
-- ----------------------------
DROP TABLE IF EXISTS "public"."campaign";
CREATE TABLE "public"."campaign" (
	"id" int4 NOT NULL DEFAULT nextval('campaign_id_seq'::regclass),
	"description" varchar(256) COLLATE "default",
	"reference" varchar(32) NOT NULL COLLATE "default",
	"campaign_start" timestamp(6) NULL,
	"campaign_end" timestamp(6) NULL,
	"owner_id" int4,
	"is_cancelled" bool DEFAULT false,
	"status" int4 DEFAULT 0,
	"request_data" varchar(4096) COLLATE "default",
	"campaign_type" varchar(256) COLLATE "default",
	"report_created" bool DEFAULT false,
	"is_test" bool DEFAULT false,
	"filename" varchar(256) COLLATE "default",
	"cps" float4,
	"calls" int4
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."campaign" OWNER TO "telephonist";

-- ----------------------------
--  Table structure for calls
-- ----------------------------
DROP TABLE IF EXISTS "public"."calls";
CREATE TABLE "public"."calls" (
	"id" int4 NOT NULL DEFAULT nextval('calls_id_seq'::regclass),
	"callsid" varchar(64) COLLATE "default",
	"call_start" timestamp(6) NOT NULL,
	"call_end" timestamp(6) NULL,
	"result" int4 DEFAULT 0,
	"destination" varchar(64) COLLATE "default",
	"campaign" varchar(32) COLLATE "default",
	"request_data" varchar(4096) COLLATE "default",
	"uuid" varchar(64) NOT NULL COLLATE "default",
	"status" varchar(32) COLLATE "default",
	"duration" int4,
	"recording_file" varchar(256) COLLATE "default",
	"recording_url" varchar(256) COLLATE "default",
	"recording_duration" int4,
	"recording_text" varchar(256) COLLATE "default",
	"recording_download" bool DEFAULT false,
	"description" varchar(256) COLLATE "default",
	"analysis" varchar(2048) COLLATE "default",
	"is_fax" bool DEFAULT false,
	"is_test" bool,
	"hangup_cause" varchar(64) COLLATE "default",
	"media_detected" bool DEFAULT false,
	"sip_callid" varchar(64) COLLATE "default",
	"hangup_cause_override" varchar(64) COLLATE "default",
	"is_blacklisted" bool DEFAULT false,
	"sip_error" varchar(128) COLLATE "default",
	"hangup_cause_report" varchar(128) COLLATE "default",
	"summary" varchar(1024) COLLATE "default"
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."calls" OWNER TO "telephonist";

-- ----------------------------
--  Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."api_user_id_seq" RESTART 2 OWNED BY "api_user"."id";
ALTER SEQUENCE "public"."calls_id_seq" RESTART 2559662 OWNED BY "calls"."id";
ALTER SEQUENCE "public"."campaign_id_seq" RESTART 903 OWNED BY "campaign"."id";
-- ----------------------------
--  Primary key structure for table api_user
-- ----------------------------
ALTER TABLE "public"."api_user" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table campaign
-- ----------------------------
ALTER TABLE "public"."campaign" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Primary key structure for table calls
-- ----------------------------
ALTER TABLE "public"."calls" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Uniques structure for table calls
-- ----------------------------
ALTER TABLE "public"."calls" ADD CONSTRAINT "calls_uuid_key" UNIQUE ("uuid") NOT DEFERRABLE INITIALLY IMMEDIATE;


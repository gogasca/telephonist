import platform

import sys
from kombu import Exchange, Queue

import settings

if platform.system() == 'Linux':
    sys.path.append('/usr/src/telephonist/')
else:
    sys.path.append('/Users/gogasca/Documents/OpenSource/Development/Python/telephonist/')

CELERYD_CHDIR = settings.filepath
CELERY_ENABLE_UTC = True
CELERY_TIMEZONE = "US/Eastern"
CELERY_ACCEPT_CONTENT = ['json', 'pickle', 'yaml']
CELERY_IGNORE_RESULT = True
CELERY_RESULT_BACKEND = "amqp"
CELERY_RESULT_PERSISTENT = True
BROKER_URL = 'amqp://telephonist:telephonist@rabbitmq:5672'
BROKER_CONNECTION_TIMEOUT = 15
BROKER_CONNECTION_MAX_RETRIES = 5

CELERY_DISABLE_RATE_LIMITS = False
CELERY_DEFAULT_RATE_LIMIT = "1/s"

CELERY_TASK_RESULT_EXPIRES = 7200
CELERY_IMPORTS = ("callcontrol.modules.telephonist")

CELERY_DEFAULT_QUEUE = "default"
CELERY_QUEUES = (
    Queue('default', Exchange('default'), routing_key='default'),
    Queue('gold', Exchange('telephonist'), routing_key='telephonist.gold'),
    Queue('silver', Exchange('telephonist'), routing_key='telephonist.silver'),
    Queue('bronze', Exchange('telephonist'), routing_key='telephonist.bronze'),
)
CELERY_DEFAULT_EXCHANGE = "telephonist"
CELERY_DEFAULT_EXCHANGE_TYPE = "topic"
CELERY_DEFAULT_ROUTING_KEY = "default"
CELERY_TRACK_STARTED = True

CELERY_ROUTES = {
    'process_call'     : {'queue': 'gold', 'routing_key': 'telephonist.gold', 'exchange': 'telephonist',},
    'process_recording': {'queue': 'silver', 'routing_key': 'telephonist.silver', 'exchange': 'telephonist',},
    'process_campaign' : {'queue': 'bronze', 'routing_key': 'telephonist.bronze', 'exchange': 'telephonist',}
}

CELERY_ANNOTATIONS = {'*': {'rate_limit': '1/s'}}

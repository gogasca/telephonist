import urllib2
folder = '/tmp/'
recording_url = 'http://api.twilio.com/2010-04-01/Accounts/AC64861838b417b555d1c8868705e4453f/Recordings/RE67bce6b7c8c65eca41a7c2f16b9afc7c'
file_name = recording_url.split('/')[-1]
u = urllib2.urlopen(recording_url)
file_name = folder + file_name
f = open(file_name, 'wb')
meta = u.info()
file_size = int(meta.getheaders("Content-Length")[0])
print "Downloading: %s Bytes: %s" % (file_name, file_size)

file_size_dl = 0
block_sz = 8192
while True:
    buffer = u.read(block_sz)
    if not buffer:
        break

    file_size_dl += len(buffer)
    f.write(buffer)
    status = r"%10d  [%3.2f%%]" % (file_size_dl, file_size_dl * 100. / file_size)
    status = status + chr(8) * (len(status) + 1)
    print status

f.close()

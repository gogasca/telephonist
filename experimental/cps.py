import random
import csv
import sys
import platform
from conf import settings
if platform.system() == 'Linux':
    sys.path.append('/usr/local/src/telephonist/application/telephonist/')
else:
    sys.path.append('/Users/gogasca/Documents/OpenSource/Development/freeswitch.git/libs/esl/python')

import ESL
import time


def random_numbers():
    """

    :return:
    """
    return random.randint(1, 1000)


def generate_list(limit):
    """

    :param limit:
    :return:
    """
    num_list = list()
    for x in xrange(1, limit + 1):
        num_list.append(random_numbers() * x)

    return num_list


def limiter(records=None, cps=settings.max_cps):
    """

    :param esl_instance:
    :param records:
    :param cps:
    :return:
    """
    try:
        start = time.time()
        if cps != 0 and cps < 1000:
            delay = 1000 / cps
        else:
            return

        print 'CPS: {} Delay: {} secs'.format(cps, float(delay) / 1000)
        while True:
            time.sleep(float(delay) / 1000)
            esl_instance = ESL.ESLconnection('104.236.190.206', '8021', 'ClueCon')
            try:
                if esl_instance.connected():
                    to = "+1" + records.pop()
                    uuid = esl_instance.api("create_uuid").getBody()

                command = "originate {origination_uuid=" + uuid + \
                          ",originate_timeout=18,origination_caller_id_number=+18623079305}sofia/gateway/asterisk/" \
                          + str(to) + " &park()"
                print command
                esl_instance.bgapi(command)

            except IndexError:
                break

        end = time.time()
        print 'Took {} secs'.format(end - start)

    except KeyboardInterrupt:
        pass


records = csv.reader(open('/tmp/test100.txt'))
records = [y for x in list(records) for y in x]
limiter(records=records)

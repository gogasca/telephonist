# location of fpcalc (leave blank if found in $PATH)
fpcalc_loc = './'
# directories to compare files between
# set to the same directory to compare files between themselves
dir_a = '/Users/gogasca/Downloads/fax/'
dir_b = '/Users/gogasca/Downloads/fax2/'
# file to write matches to
match_file = 'matches.txt'
# seconds to sample audio file for Default 500
sample_time = 20
# number of points to crop from beginning of fingerprint
# 4096 samples per frame / 11025 samples per second / 3 points per frame
# = 0.124 seconds per point
# crop 1100
crop = 0
# number of points to scan cross correlation over
# span 1200
span = 20
# step size (in points) of cross correlation
step = 3
# report match when cross correlation has a peak exceeding threshold
threshold = 0.6
# minimum number of points that must overlap in cross correlation
# exception is raised if this cannot be met
min_overlap = 100

################################################################################
# import modules
################################################################################

import commands
import math
import re

import numpy


################################################################################
# function definitions
################################################################################

# adds escape characters in front of Bash special characters
def esc_bash_chars(string):
    # match any of the following characters between the capital A's
    # A`!$^&*()=[]{}\|;:'",<>? A
    # note that characters ] and ' need escape characters themselves in the
    # regex, and \ requires two escape characters
    specialchars = re.compile('[`!$^&*()=[\]{}\\\|;:\'",<>? ]')
    string_escaped = ""
    for char in string:
        if specialchars.search(char):
            string_escaped += '\\' + char
        else:
            string_escaped += char
    return string_escaped


# returns variance of list
def variance(listx):
    meanx = numpy.mean(listx)
    # get mean of x^2
    meanx_sqr = 0
    for x in listx:
        meanx_sqr += x ** 2
    meanx_sqr = meanx_sqr / float(len(listx))
    return meanx_sqr - meanx ** 2


# returns correlation between lists
def correlation(listx, listy):
    """

    :param listx:
    :param listy:
    :return:
    """
    if len(listx) == 0 or len(listy) == 0:
        # Error checking in main program should prevent us from ever being
        # able to get here.
        raise Exception('Empty lists cannot be correlated.')
    if len(listx) > len(listy):
        listx = listx[:len(listy)]
    elif len(listx) < len(listy):
        listy = listy[:len(listx)]

    meanx = numpy.mean(listx)
    meany = numpy.mean(listy)

    covariance = 0
    for i in range(len(listx)):
        covariance += (listx[i] - meanx) * (listy[i] - meany)
    covariance = covariance / float(len(listx))
    return covariance / (math.sqrt(variance(listx)) * math.sqrt(variance(listy)))


# return cross correlation, with listy offset from listx
def cross_correlation(listx, listy, offset):
    if offset > 0:
        listx = listx[offset:]
        listy = listy[:len(listx)]
    elif offset < 0:
        offset = -offset
        listy = listy[offset:]
        listx = listx[:len(listy)]
    if min(len(listx), len(listy)) < min_overlap:
        # Error checking in main program should prevent us from ever being
        # able to get here.
        raise Exception('Overlap too small: %i' % min(len(listx), len(listy)))
    return correlation(listx, listy)


# cross correlate listx and listy with offsets from -span to span
def compare(listx, listy, span, step):
    """

    :param listx:
    :param listy:
    :param span:
    :param step:
    :return:
    """
    if span > min(len(listx), len(listy)):
        # Error checking in main program should prevent us from ever being
        # able to get here.
        raise Exception('span >= sample size: %i >= %i\n'
                        % (span, min(len(listx), len(listy)))
                        + 'Reduce span, reduce crop or increase sample_time.')
    corr_xy = []
    for offset in numpy.arange(-span, span + 1, step):
        corr_xy.append(cross_correlation(listx, listy, offset))
    return corr_xy


# return index of maximum value in list
def max_index(listx):
    """

    :param listx:
    :return:
    """
    max_index = 0
    max_value = listx[0]
    for i, value in enumerate(listx):
        if value > max_value:
            max_value = value
            max_index = i
    return max_index


# write to a file
def write_string(string, filename):
    file_out = open(filename, 'ab')
    file_out.write(string + '\n')
    file_out.close()


################################################################################
# main code
################################################################################

# escape Bash special characters
dir_a = esc_bash_chars(dir_a)
dir_b = esc_bash_chars(dir_b)
match_file = esc_bash_chars(match_file)

# get list of files to compare from each directory
filelist_a = commands.getoutput('ls ' + dir_a + '*.wav').split('\n')
filelist_b = commands.getoutput('ls ' + dir_b + '*.wav').split('\n')

print filelist_a
print filelist_b
# if cross-correlating between files within a directory, don't correlate files
# twice, or correlate files with themselves
intra_correlating = False
if filelist_a == filelist_b:
    intra_correlating = True

for i, file_a in enumerate(filelist_a):
    # if correlating between files within a directory, set filelist_b such that
    # cross-correlations are not repeated, and files are not correlated with
    # themselves
    if intra_correlating:
        # remove files already correlated with from filelist_b, along with
        # current file
        filelist_b = filelist_a[i + 1:]
        if len(filelist_b) == 0:
            # nothing left to check!
            break

    file_a = esc_bash_chars(file_a)
    # calculate fingerprint
    fpcalc_out = commands.getoutput('%sfpcalc -raw -length %i %s'
                                    % (fpcalc_loc, sample_time, file_a))

    fingerprint_index = fpcalc_out.find('FINGERPRINT=') + 12
    # convert fingerprint to list of integers
    fingerprint_a = map(int, fpcalc_out[fingerprint_index:].split(','))
    print "Result: {} \n\rIndex: {} ".format(fpcalc_out, fingerprint_index)
    print "Len: {} Crop: {} Span: {} Min Overlap {}".format(len(fingerprint_a), crop, span, min_overlap)
    print "Len - Crop - Span: {} ".format(len(fingerprint_a) - crop - span)
    # Check that Fingerprint length meets minimum size
    if len(fingerprint_a) - crop - span < min_overlap:
        raise Exception('Fingerprint length less than required:\n'
                        + 'File: %s\n' % file_a
                        + 'Fingerprint length: %i\n' % len(fingerprint_a)
                        + 'Required length (crop + span + min_overlap): %i\n'
                        % (crop + span + min_overlap)
                        + 'Increase sample_time, reduce span or reduce crop.')

    for file_b in filelist_b:
        file_b = esc_bash_chars(file_b)
        # calculate fingerprint
        print "Calculate fingerprint for {}".format(file_b)

        fpcalc_out = commands.getoutput('%sfpcalc -raw -length %i %s'
                                        % (fpcalc_loc, sample_time, file_b))
        fingerprint_index = fpcalc_out.find('FINGERPRINT=') + 12
        # convert fingerprint to list of integers
        fingerprint_b = map(int, fpcalc_out[fingerprint_index:].split(','))

        print len(fingerprint_b), crop, span, min_overlap
        # Check that fingerprint length meets minimum size
        if len(fingerprint_b) - crop - span < min_overlap:
            raise Exception('Fingerprint length less than required:\n'
                            + 'File: %s\n' % file_b
                            + 'Fingerprint length: %i\n' % len(fingerprint_b)
                            + 'Required length (crop + span + min_overlap): %i\n'
                            % (crop + span + min_overlap)
                            + 'Increase sample_time, reduce span or reduce crop.')

        # cross correlation between fingerprints
        corr_ab = compare(fingerprint_a[crop:], fingerprint_b[crop:], span, step)

        max_corr_index = max_index(corr_ab)
        max_corr_offset = -span + max_corr_index * step

        # report matches
        if corr_ab[max_corr_index] > threshold:
            print('%s and %s match with correlation of %.4f at offset %i'
                  % (file_a, file_b, corr_ab[max_corr_index], max_corr_offset))
            write_string('%s\t%s\t%.6f\t%i'
                         % (file_a, file_b, corr_ab[max_corr_index],
                            max_corr_offset),
                         match_file)

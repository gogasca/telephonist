import ConfigParser
import logging

from twilio.rest import TwilioRestClient

from callcontrol import Call, CallProcessor, Provider
from conf import logger

config = ConfigParser.ConfigParser()
config.read("conf/twilio.conf")


def main():
    try:

        log = logger.LoggerManager().getLogger("__app__")
        log.setLevel(level=logging.DEBUG)
        log.info("Dialer starting...")

        provider_instance = Provider.Provider()
        provider_instance.name = config.get('Provider', 'name')
        provider_instance.account_name = config.get('Provider', 'account_sid')
        provider_instance.token = config.get('Provider', 'auth_token')

        log.info(provider_instance)
        log.info("provider_instance created ")
        twilio_client = TwilioRestClient(account=provider_instance.account_name,
                                         token=provider_instance.token)

        call_instance = Call.Call()
        call_instance.client = twilio_client
        log.info("call_instance created ")

        call_info = dict()
        call_info["from"] = "+16506814252"
        call_info["to"] = "+14082186575"
        call_info["url"] = "http://137651c1.ngrok.io/record"
        call_info["status_callback"] = "http://137651c1.ngrok.io/callstatus"
        call_info["timeout"] = 18
        log.info("call_info created ")

        call_instance.call_info = call_info
        callprocessor = CallProcessor.CallProcessor()
        callprocessor.call = call_instance
        callprocessor.handle_call()

    except Exception, e:
        print e


main()

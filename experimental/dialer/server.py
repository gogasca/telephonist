__author__ = 'gogasca'

import twilio.twiml
from flask import Flask, request, jsonify

dialer = Flask(__name__)


@dialer.route('/callstatus', methods=['POST'])
def handle_callstatus():
    """

    :return:
    """
    try:
        if request:
            print ' * Status received'
            request_elements = request.args
            for key in request_elements:
                print ' * Element: ' + dict[key]
            call_sid = request.values.get('CallSID')
            called = request.values.get('Called')
            call_status = request.values.get('CallStatus')
            print call_status
            timestamp = request.values.get('Timestamp')  # e.g. Wed, 02 Mar 2016 15:42:30 +0000
            call_statuses = {
                "queued"     : "QUEUED",
                "ringing"    : "RINGING",
                "canceled"   : "CANCELED",
                "completed"  : "COMPLETED",
                "in-progress": "IN-PROGRESS",
                "failed"     : "CALL FAILED",
                "no-answer"  : "NO ANSWER",
                "busy"       : "BUSY"
            }

            print ' {} {} {}'.format(timestamp, call_statuses[call_status], called)
            return jsonify({"status": 200})
        else:
            print 'No response found'

    except Exception, e:
        print e  # Main


@dialer.route("/recordresult", methods=['GET', 'POST'])
def handle_recordingresult():
    """Play back the caller's recording."""
    print ' * Recording Result'
    response = twilio.twiml.Response()
    recording_url = request.values.get("RecordingUrl", None)
    recording_duration = request.values.get("RecordingDuration", None)
    digits = request.values.get("Digits", None)
    callSid = request.values.get("CallSid", None)
    print callSid
    print recording_url
    print recording_duration
    print digits
    # TODO Process recording: Download recording and create transcription
    return str(response)


@dialer.route('/record', methods=['POST'])
def handle_recording(record=True):
    try:
        response = twilio.twiml.Response()
        # response.say(" ", voice="alice")
        if record:
            print ' * Recording'
            response.record(maxLength=20, playBeep=False, trim="do-not-trim", timeout=18, action="/recordresult")
            return str(response)

        caller = request.values.get("Caller")
        print caller
        return str(response)
    except ValueError, e:
        print e


@dialer.route('/incoming', methods=['POST'])
def handle_incoming_call(record=False):
    try:
        print ' * Incoming'
        # TODO Process incoming call. Update database with call status
        response = twilio.twiml.Response()
        response.say("Welcome to Telephonist", voice="alice")
        if record:
            print ' * Recording'
            response.record(maxLength=20, playBeep=False, trim="do-not-trim", timeout=18, action="/recordresult")
            return str(response)
        caller = request.values.get("Caller")
        callSid = request.values.get("CallSid")
        print caller, callSid
        return str(response)
    except ValueError, e:
        print e


def main():
    try:
        print ' * Starting telephonist server'
        dialer.run(host='0.0.0.0', port=3000, debug=True, use_reloader=False)

    except Exception, e:
        print 'Exception found during app initialization ' + str(e)


if __name__ == "__main__":
    main()

from tornado import ioloop, httpclient
import datetime
import time
i = 0

def handle_request(response):

    print response.code,
    print datetime.datetime.now()
    global i
    i -= 1
    if i == 0:
        ioloop.IOLoop.instance().stop()

http_client = httpclient.AsyncHTTPClient()
for url in open('urls.txt'):
    i += 1
    http_client.fetch(url.strip(), handle_request, method='HEAD')
ioloop.IOLoop.instance().start()
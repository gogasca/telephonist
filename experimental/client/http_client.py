class CallRequest(RequestHandler):
    def __init__(self, call_request):
        self.call_request = call_request

    @staticmethod
    def handle_request(response):
        """

        :param response:
        :return:
        """
        if response.code != 202:
            helper.failed_requests(response)
            # IOLoop.instance().stop()

    @gen.coroutine
    def post(self):
        try:
            http_client = httpclient.AsyncHTTPClient()
            log.info("_process() Call request: {}".format(self.call_request))
            headers = {'Content-Type': 'application/json'}
            request = httpclient.HTTPRequest(auth_username=settings.api_account,
                                             auth_password=settings.api_password,
                                             url=settings.api_call_url,
                                             body=json.dumps(self.call_request),
                                             headers=headers,
                                             request_timeout=15,
                                             method="POST")
            response = yield http_client.fetch(request, self.handle_request)
            log.info("_process() Call request to: {} API Response: {}".format(self.call_request["to"],
                                                                              response.code))

        except httpclient.HTTPError, e:
            log.exception("_process() " + str(e))
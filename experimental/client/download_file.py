
import urllib2
import time

url = 'http://104.236.190.206:8081/JJLYAAP6FU_4082186575_8c33921c-j1gt-3x14-uja5-3g6q-8c339eb855.wav'
myfile = '/tmp/recordings/' + url.split('/')[-1]
print myfile
start = time.time()
req = urllib2.Request(url, headers={'User-Agent': 'Mozilla/5.0'})
u = urllib2.urlopen(req)
f = open(myfile, 'wb')
meta = u.info()
file_size = int(meta.getheaders("Content-Length")[0])
print("CallRecording() Downloading: %s Bytes: %s" % (myfile, file_size))
file_size_dl = 0
block_sz = 8192
while True:
    buffer = u.read(block_sz)
    if not buffer:
        break

    file_size_dl += len(buffer)
    f.write(buffer)
    status = r"%10d  [%3.2f%%]" % (file_size_dl, file_size_dl * 100. / file_size)
    status += chr(8) * (len(status) + 1)
    print status

end = time.time()
print('CallRecording() Download took: {} secs '.format(end - start))
f.close()

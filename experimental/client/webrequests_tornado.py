import time
from tornado.ioloop import IOLoop
from tornado import gen
from tornado import ioloop, httpclient

def my_function(callback):
    print 'do some work'
    # Note: this line will block!
    time.sleep(1)
    print time.time()
    callback(123)

def handle_request(response):
    print(response.code)
    global i
    i -= 1
    if i == 0:
        ioloop.IOLoop.instance().stop()

@gen.engine
def f():
    print 'start'
    # Call my_function and return here as soon as "callback" is called.
    # "result" is whatever argument was passed to "callback" in "my_function".

    http_client = httpclient.AsyncHTTPClient()
    for url in open('urls.txt'):

        http_client.fetch(url.strip(), handle_request, method='HEAD')
    ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    f()
    IOLoop.instance().start()
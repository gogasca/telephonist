# coding=utf-8
import platform
import sys

if platform.system() == 'Linux':
    sys.path.append('/usr/local/src/telephonist/application/telephonist/')
else:
    sys.path.append('/Users/gogasca/Documents/OpenSource/Development/freeswitch.git/libs/esl/python')

import ESL
import time


def call_freeswitch(destination, freeswitch_gateway):
    """

    :param destination:
    :param freeswitch_gateway:
    :return:
    """
    try:
        esl_instance = ESL.ESLconnection('104.236.190.206', '8021', 'ClueCon')
        if esl_instance.connected():
            esl_instance.events("plain",
                                "CHANNEL_OUTGOING CHANNEL_STATE MESSAGE_WAITING CHANNEL_CREATE CHANNEL_PROGRESS_MEDIA CHANNEL_CALLSTATE BACKGROUND_JOB CHANNEL_HANGUP")
        else:
            print 'call_freeswitch() ESL instance not connected'
            return

        uuid = esl_instance.api("create_uuid").getBody()

        print 'call_freeswitch() Created UUID: {}'.format(uuid)

        command = "originate {origination_uuid=" + \
                  uuid + ",originate_timeout=18,origination_caller_id_number=+18623079305}sofia/external/" + \
                  destination + "@" + \
                  freeswitch_gateway + " & park()"

        res = esl_instance.bgapi(command)
        job_uuid = res.getHeader("Job-UUID")

        print 'call_freeswitch() Call to: ' + command + ' has Job-UUID {} Call-UUID {}'.format(job_uuid, uuid)
        oldtime = time.time()

        print 'call_freeswitch() Contacting Freeswitch checking UUID...'
        uuid_exists = esl_instance.api('uuid_exists ' + uuid).getBody()
        time.sleep(0.25)
        if not uuid_exists:
            print 'call_freeswitch() Call has not been created UUID {}'.format(uuid)
            return
        else:
            print 'call_freeswitch() UUID created: {}'.format(uuid)

        while time.time() - oldtime < 60:
            print 'call_freeswitch() Waiting for Freeswitch events...'
            reply = esl_instance.recvEvent()
            if reply:
                ev_name = reply.getHeader('Event-Name')
                print 'call_freeswitch() Event Name: {}'.format(ev_name)
                if ev_name == 'BACKGROUND_JOB':
                    if reply.getHeader("Job-UUID") == job_uuid:
                        call_result = reply.getBody()
                        print "Result of call to was: {}".format(call_result)

                elif ev_name == 'CHAN_NOT_IMPLEMENTED':
                    print ev_name
                elif ev_name == 'CHANNEL_CALLSTATE':
                    print ev_name
                    if reply.getHeader("Channel-Call-State") == 'ACTIVE':
                        print 'call_freeswitch() Call has been answered'
                        print reply.serialize()
                elif ev_name == 'CHANNEL_STATE':
                    channel_status = reply.getHeader("Channel-Call-State")
                    print "call_freeswitch() Channel status: {}".format(channel_status)
                elif ev_name == 'CHANNEL_HANGUP':
                    print ev_name
                    print 'call_freeswitch() Call has been terminated'
                    print reply.serialize()
                    break
            else:
                print 'call_freeswitch() Waiting...'

        print 'call_freeswitch() Kill UUID {}'.format(uuid)
        esl_instance.api("uuid_kill", uuid)
        esl_instance.disconnect()

    except KeyboardInterrupt, exception:
        print 'Exception {}'.format(exception)


call_freeswitch("+14082186575", "telephonist.pstn.twilio.com")

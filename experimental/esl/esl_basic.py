# coding=utf-8
import platform
import sys

if platform.system() == 'Linux':
    sys.path.append('/usr/local/src/telephonist/application/telephonist/')
else:
    sys.path.append('/Users/gogasca/Documents/OpenSource/Development/freeswitch.git/libs/esl/python')
import ESL

freeswitch_gateway = "telephonist.pstn.twilio.com"
destination = "+14082186575"
command = "originate"
con = ESL.ESLconnection('104.236.160.212', '8021', 'ClueCon')
uuid = con.api("create_uuid").getBody()
print 'Created UUID: {}'.format(uuid)
command = "originate {origination_uuid=" + uuid + ",originate_timeout=18,origination_caller_id_number=+18623079305}sofia/external/" + destination + "@" + freeswitch_gateway + " & park()"

res = con.bgapi(command )
job_uuid = res.getHeader("Job-UUID")
print 'Call to ' + command + ' has Job-UUID {} Call-UUID {}'.format(job_uuid, uuid)
stay_connected = True

while stay_connected:
    e = con.recvEventTimed(20)
    if e:
        print e.serialize()
        ev_name = e.getHeader('Event-Name')
        if ev_name == 'BACKGROUND_JOB':
            if e.getHeader("Job-UUID") == job_uuid:
                call_result = e.getBody()
                print "Result of call to $target was $call_result\n\n"
        elif ev_name == 'DTMF':
            stay_connected = False
        elif ev_name == 'CHAN_NOT_IMPLEMENTED':
            print ev_name
    else:
        import time
        time.sleep(1)
        print 'Waiting...'


#con.api("uuid_kill",uuid)
from tornado.log import gen_log
from tornado import gen, httpclient, ioloop
import csv
import tornado.web
import time
from functools import partial

#https://github.com/wpjunior/tornado-retry-client/blob/master/tests/test_retry_client.py

def asynchronous_fetch(url, callback):
    http_client = httpclient.AsyncHTTPClient()

    def handle_response(response):
        callback(response.body)

    http_client.fetch(url, callback=handle_response)


class GenAsyncHandler(tornado.web.RequestHandler):
    @gen.coroutine
    def get(self):
        http_client = httpclient.AsyncHTTPClient()
        response = yield http_client.fetch("http://0.0.0.0:8080/api/1.0/echo")
        do_something_with(response)

def handle_response(number, response):
    """

    :param number:
    :param response:
    :return:
    """
    if response.error:
        result = number + "," + str(response.code) + "\n"
        print result

def asynchronous_fetch(url, call_record):
    http_client = httpclient.AsyncHTTPClient()
    request = httpclient.HTTPRequest(
        url=url,
        request_timeout=5,
        method="POST")
    print url
    http_client.fetch(url, callback=do_something_with)


def do_something_with(response):
    print response.code


@gen.coroutine
def start_campaign():
    call_records = csv.reader(open('/tmp/test10.us'))
    for call_record in call_records:
        time.sleep(0.1)
        asynchronous_fetch("http://0.0.0.0:8080/api/1.0/echo", call_record)


def main():
    ioloop.IOLoop.current().run_sync(start_campaign)


if __name__ == "__main__":
    main()

import freeswitch
import psycopg2


class FreeswitchDb(object):
    """

    """

    def __init__(self,
                 server='127.0.0.1',
                 username='telephonist',
                 password='telephonist',
                 database='telephonistdb',
                 port=5432, **kwargs):
        """

        :param server:
        :param username:
        :param password:
        :param database:
        :param port:
        :param kwargs:
        :return:
        """
        self.server = server
        self.username = username
        self.password = password
        self.database = database
        self.port = port
        self.dsn = 'dbname=%s host=%s port=%d user=%s password=%s' % (self.database,
                                                                      self.server,
                                                                      self.port,
                                                                      self.username,
                                                                      self.password)
        self.conn = None
        self.initialize(self.dsn)

    def initialize(self, dsn=None):
        """

        :param dsn:
        :param kwargs:
        :return:
        """
        try:
            if self.dsn:
                self.conn = psycopg2.connect(self.dsn)
            elif dsn:
                self.conn = psycopg2.connect(dsn)
            else:
                raise psycopg2.DatabaseError('Invalid DSN.')

            return True
        except psycopg2.OperationalError as exception:
            freeswitch.consoleLog('error', 'Database OperationalError: {}'.format(exception))

    def update(self, query):
        """

        :param query:
        :return:
        """
        try:
            if self.conn:
                cur = self.conn.cursor()
                cur.execute(query)
                self.conn.commit()
            else:
                freeswitch.consoleLog('error', 'No connection to DB')
                return
        except psycopg2.ProgrammingError as exception:
            freeswitch.consoleLog('error', 'Database ProgrammingError: {}'.format(exception))
        except Exception as exception:
            freeswitch.consoleLog('error', 'Exception unable to query: {}: {}'.format(exception))
        finally:
            if self.conn:
                self.conn.close()
            if cur:
                cur.close()

def handler(session, args):
    """

    :param session:
    :param args:
    :return:
    """
    server = "104.236.173.109"
    username = "telephonist"
    password = "telephonist"
    database = "telephonistdb"
    port = 5432
    caller = session.getVariable("caller")
    uuid = session.getVariable("uuid")
    #with open("/tmp/fax.txt", "a") as myfile:
    #    myfile.write("Caller name {} UUID: {}\n".format(caller, uuid))
    callinstance = FreeswitchDb(server=server,
                                username=username,
                                password=password,
                                database=database,
                                port=port)

    if uuid:
        callinstance.update("UPDATE calls SET is_fax=true WHERE calls.uuid='" + uuid + "'")
        freeswitch.consoleLog('info', 'Database was updated')
    else:
        freeswitch.consoleLog('error', 'Not a valid UUID')


def fsapi(session, stream, env, args):
    pass

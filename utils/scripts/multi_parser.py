import re
import time

from utils import helper


def find_uuid(filename):
    """

    :param filename:
    :return:
    """
    uuidList = list()
    with open(filename) as f:
        for line in f:
            uuid = line.rstrip('\n')
            if uuid:
                uuidList.append(uuid)
    return uuidList


def find_fields(filename):
    """

    :param filename:
    :return:
    """
    recordList = list()
    with open(filename) as f:
        for line in f:
            recordLine = line.rstrip('\n')
            record = re.search(r'([\-]?\d),(\d+),([\w.\-]+),([\w\_]+)', recordLine)
            if record:
                result = record.group(1)
                number = record.group(2)
                status = record.group(3)
                hangup_cause = record.group(4)
                print result, number, status, hangup_cause
                normalize_calls(result, number, status, hangup_cause)
                record = None
                recordList.append(number)
            else:
                print line

    return recordList


def find_recordings(filename):
    """

    :param filename:
    :return:
    """
    recordingList = list()
    with open(filename) as f:
        for line in f:
            recordingLine = line.rstrip('\n')
            uuid = re.search(r'(^\w+)_(.*)', recordingLine)
            if uuid:
                uuidValue = uuid.group(2)
                uuid = None
            recordingList.append(uuidValue)
    return recordingList


def find_errors(filename):
    errorList = list()
    with open(filename) as f:
        for line in f:
            errorLine = line.rstrip('\n')
            uuid = re.search(r'(^\w+-\w+-\w+\w+-\w+-\w+-\w+)|(.*)', errorLine)
            if uuid:
                uuidValue = uuid.group(1)
                uuid = None
            errorList.append(uuidValue)
    return errorList


def find_numbers(filename, uuid_list):
    """

    :param filename:
    :return:
    """
    numberList = list()
    with open(filename) as f:
        for line in f:
            numberLine = line.rstrip('\n')
            number = re.search(r'(\d+),(\w+-\w+-\w+\w+-\w+-\w+-\w+)', numberLine)
            if number:
                numberValue = number.group(1)
                if number.group(2) in uuid_list:
                    numberList.append(numberValue)
                number = None
        return numberList


def find_numbers_uuids(filename):
    """

    :param filename:
    :return:
    """
    uuidList = list()
    with open(filename) as f:
        for line in f:
            uuidLine = line.rstrip('\n')
            uuid = re.search(r'(\d+),(\w+-\w+-\w+\w+-\w+-\w+-\w+)', uuidLine)
            if uuid:
                uuidValue = uuid.group(2)
                uuid = None
            uuidList.append(uuidValue)
        return uuidList


def symmetric_difference(a, b):
    """
    return symmetric_difference (a U b) - (a n b)
    :param a:
    :param b:
    :return:
    """
    return list(set(a) ^ set(b))


def intersect(a, b):
    """
    return the intersection of two lists
    :param a:
    :param b:
    :return:
    """
    return list(set(a) & set(b))


def update_calls(calls):
    """

    :param calls:
    :return:
    """

    for uuid in calls:
        sqlquery = """UPDATE calls SET status='failed',result=-1,hangup_cause_override='INVALID_NUMBER',hangup_cause_report='NLIS' WHERE calls.uuid='""" + uuid + "'"
        print sqlquery
        helper.update_database(sqlquery)
        time.sleep(0.15)


def normalize_calls(result, number, status, hangup_cause):
    """

    :param result:
    :param number:
    :param status:
    :param hangup_cause:
    :return:
    """
    if result and number and status and hangup_cause:
        sqlquery = """UPDATE calls SET status='""" + status + "',result=" + str(
            result) + ",hangup_cause_override='" + hangup_cause + "' WHERE campaign='HGWHML2RQP' and destination LIKE '%" + number + "'"
        helper.update_database(sqlquery)
        print sqlquery
        time.sleep(0.15)


if __name__ == "__main__":

    # filename = '/Users/gogasca/Downloads/telephonist_logs/NBLQJNQBQ7.translate_errors'
    filename = '/Users/gogasca/Downloads/SGCA3PX44P.csv'

    if filename:
        failed_calls = find_uuid(filename)
        print len(failed_calls)
        update_calls(failed_calls)
        # uuid_numbers = find_numbers_uuids('/Users/gogasca/Downloads/HGWHML2RQP.all')
        # print len(uuid_numbers)
        # cps_error = find_errors('/Users/gogasca/Downloads/HGWHML2RQP.cps')
        # print len(cps_error)
        # print cps_error
        # cps_calls_with_error = intersect(cps_error,uuid_numbers)
        # numbers_with_error = find_numbers('/Users/gogasca/Downloads/HGWHML2RQP.all', cps_calls_with_error)
        # for number in numbers_with_error:
        #    print number[2:]
        # recordings = find_recordings('/Users/gogasca/Downloads/HGWHML2RQP.recordings')
        # errors = find_errors('/Users/gogasca/Downloads/HGWHML2RQP.twilioerror')
        # translations = find_uuid('/Users/gogasca/Downloads/HGWHML2RQP.translated')
        # cps = find_fields(filename)
        # print len(cps)
        # update_calls(failed_calls)
        # print 'Recordings found: {}'.format(len(recordings))
        # print 'Translated calls: {}'.format(len(translations))
        # print 'Twilio Termination MVTS: {}'.format(len(errors))
        # print 'Twilio Errors with Translated file: {}'.format(len(intersect(failed_calls, errors)))
        # print 'Twilio Errors without translation: {}'.format(
        #    len(symmetric_difference(errors, intersect(failed_calls, errors))))
        # invalid_calls = symmetric_difference(errors, intersect(failed_calls, errors))

"""
"""

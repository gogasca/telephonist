#!/usr/bin/env bash

# Backup Telephonist API logs

DIRECTORY="ZQFQ5BH03A"
echo "Backing up logs..."
mkdir /var/log/telephonist/logs/backup/$DIRECTORY
cd /var/log/telephonist/logs/backup/
cp /usr/src/telephonist/log/apid.log $DIRECTORY
cp /usr/src/telephonist/log/celeryd1.log $DIRECTORY
cp /usr/src/telephonist/log/celeryd2.log $DIRECTORY
> /usr/src/telephonist/log/apid.log
> /usr/src/telephonist/log/celeryd1.log
> /usr/src/telephonist/log/celeryd2.log
ls -al /usr/src/telephonist/log/
echo "Backup completed"
# coding=utf-8

import time
import csv
import psycopg2

mexico_errors = [
    'disponible o se encuentra fuera del',
    'el nÃºmero no es vÃ¡lido',
    'en servicio por favor verifique el numero y vuelve a intentarlo',
    'fuera de servicio no es necesario que lo reporte',
    'fuera de servicio no es necesario que lo reporte al 050 gracias',
    'marcado no permite completar la llamada consulte su agenda',
    'que ha marcado se encuentra fuera de servicio por favor verifique el numero',
    'que ha marcado CE encuentra fuera de servicio por favor verifique el numero',
    'que ha marcado se encuentra fuera de servicio por favor verifica el',
    'que usted marco ha cambiado ahora es el',
    'que usted marco cambio el nuevo',
    'que usted marco no es v',
    'que usted marco no existe favor de rectificarlo',
    'que usted marco no existe por de verificarlo',
    'suspendido por lo que no es necesario reportarlo',
    'que usted marco se encuentra',
    'que usted marco no existe favor de verificarlo Gracias'
    'que usted marco cambio el nuevo', ]


class Db():
    """
        Database handling
    """

    dbhost = 'cluster1-db1-sp1-aws.czcjx1n3is2u.sa-east-1.rds.amazonaws.com'
    dbport = 5432
    dbusername = 'telephonist'
    dbpassword = 'telephonist'
    dbname = 'telephonistdb'

    # =========================================================
    # SQLALCHEMY
    # =========================================================

    # "postgresql://telephonist:telephonist@198.199.110.24/telephonistdb
    SQLALCHEMY_DATABASE_URI = "postgresql://" + dbusername + ":" + dbpassword + "@" + dbhost + "/" + dbname
    SQLALCHEMY_DSN = 'dbname=' + dbname + \
                     ' host=' + dbhost + \
                     ' port=' + str(dbport) + \
                     ' user=' + dbusername + \
                     ' password=' + dbpassword

    def __init__(self,
                 server=dbhost,
                 username=dbusername,
                 password=dbpassword,
                 database=dbname,
                 port=dbport, **kwargs):
        """

        :param server:
        :param username:
        :param password:
        :param database:
        :param port:
        :param kwargs:
        :return:
        """
        self.server = server
        self.username = username
        self.password = password
        self.database = database
        self.port = port
        self.dsn = 'dbname=%s host=%s port=%d user=%s password=%s' % (self.database,
                                                                      self.server,
                                                                      self.port,
                                                                      self.username,
                                                                      self.password)
        self.conn = None

    def initialize(self, dsn=None, **kwargs):
        """

        :param dsn:
        :param kwargs:
        :return:
        """

        if self.dsn:
            self.conn = psycopg2.connect(self.dsn)
            return True
        elif dsn:
            self.conn = psycopg2.connect(dsn)
            return True
        else:
            print ('DB initialize() DB not initialized')
            raise RuntimeError('Invalid dsn.')

    def update(self, query):
        """

        :param query:
        :return:
        """
        try:
            if self.conn:
                cur = self.conn.cursor()
                cur.execute(query)
                self.conn.commit()
            else:
                print 'No connection to DB'
                return
        except psycopg2.ProgrammingError as exception:
            print exception
            raise
        except Exception, exception:
            print 'Unable to query {}'.format(query)
            print exception
            raise

    def terminate(self):
        if self.conn:
            self.conn.close()


class FileReader(object):
    """

    """

    def __init__(self, filename):
        self._filename = filename

    @property
    def filename(self):
        return self._filename

    @filename.setter
    def filename(self, filename):
        self._filename = filename

    def read(self):
        try:
            with open(self.filename, 'rU') as csvfile:
                records = csv.reader(csvfile, delimiter=',', quotechar='"', dialect=csv.excel_tab)
                for record in records:
                    print record[2]
        except IndexError as e:
            print('File %s, Line %d: %s' % (self.filename, records.line_num, e))
        except csv.Error as e:
            print('File %s, Line %d: %s' % (self.filename, records.line_num, e))


def process_csv(filename):
    """

    :param filename:
    :return:
    """
    filereader = FileReader(filename)
    filereader.read()


def find_uuid(filename):
    """

    :param filename:
    :return:
    """
    uuidList = list()
    with open(filename) as f:
        for line in f:
            uuid = line.rstrip('\n')
            if uuid:
                uuidList.append(uuid)
    return uuidList


def find_callid(filename):
    """

    :param filename:
    :return:
    """
    callidList = list()
    with open(filename) as f:
        for line in f:
            callid = line.rstrip('\n')
            if callid:
                callidList.append(callid)
    return callidList


def update_calls(calls):
    """

    :param calls:
    :return:
    """

    db = Db()
    print 'Connecting to database...'
    db.initialize()
    print 'Updating records...'
    for call in calls:
        sqlquery = """UPDATE calls SET hangup_cause_override='NO_ANSWER',hangup_cause_report='WTN' WHERE calls.sip_callid='""" + call + "'"
        db.update(sqlquery)
        print sqlquery
        time.sleep(0.05)

    db.terminate()


def find_dnus(original_file, processed_file):
    """

    :param original_file:
    :param processed_file:
    :return:
    """
    # Create dictionary of processed numbers.
    with open(processed_file, 'rb') as processed_f:
        processed_numbers = dict((r[0], r[1]) for i, r in enumerate(csv.reader(processed_f)))

    total_rows = 0
    # Open original file and look for processed numbers. (Linear)
    with open(original_file, 'rb') as original_f:
        with open('results.csv', 'wb') as results:
            original_file_reader = csv.reader(original_f)
            writer = csv.writer(results)

            for row in original_file_reader:
                index = processed_numbers.get(row[2])
                if index is not None:
                    # Found
                    writer.writerow(row + [processed_numbers[row[2]]])
                    total_rows += 1
                else:
                    print row[2]

    print ('Total records processed %d ' % total_rows)

if __name__ == "__main__":
    original_file = '/Users/gogasca/Downloads/mexico_raw.csv'
    report_file = '/Users/gogasca/Downloads/mexico_report.csv'
    find_dnus(original_file, report_file)


#!/usr/bin/env bash

DIRECTORY="9KBRPQ9IK3"
cd /var/log/freeswitch
mkdir /var/log/telephonist/backup/logs/$DIRECTORY
mv freeswitch.log* /var/log/telephonist/backup/logs/$DIRECTORY
cp cdr-csv/Master.csv /var/log/telephonist/backup/logs/$DIRECTORY
> cdr-csv/Master.csv
cd /var/log/telephonist/backup/logs/$DIRECTORY
zip $DIRECTORY.zip freeswitch.log*
rm freeswitch.log*
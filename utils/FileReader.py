import csv
import datetime
import logging
import mmap
import os
import time

from conf import logger

log = logger.LoggerManager().getLogger("__app__")
log.setLevel(level=logging.DEBUG)


class FileReader(object):
    def __init__(self, filename):
        self._filename = filename

    @property
    def filename(self):
        return self._filename

    @filename.setter
    def filename(self, filename):
        self._filename = filename

    def read(self):
        try:
            if self.filename:
                with open(self.filename, 'rb') as csvfile:
                    telephone_numbers = csv.reader(csvfile, delimiter=',')
                    return telephone_numbers

        except csv.Error as e:
            log.exception('File %s, Line %d: %s' % (self.filename, telephone_numbers.line_num, e))


def check_fileformat(filename):
    """

    :param filename:
    :return:
    """

    records = csv.reader(open(filename))
    for record in records:
        if len(record) > 2:
            return False
    # print 'check_fileformat() File format is correct'
    return True


def add_log(filename='/tmp/bad_callrequests.txt', data=''):
    """
    Add log info to file
    :param data:
    :return:
    """
    try:

        with open(filename, "ab") as report_file:
            current_timestamp = time.time()
            timestamp = datetime.fromtimestamp(current_timestamp).strftime('%Y-%m-%d %H:%M:%S')
            report_file.write(timestamp + " " + data + '\r\n')
            report_file.close()

    except Exception, excpt:
        log.exception(excpt)


def file_exists(filename):
    """

    :param filename:
    :return:
    """
    try:
        return os.path.isfile(filename)
    except Exception:
        return False


def mapcount(filename):
    """

    :param filename:
    :return:
    """
    f = open(filename, "r+")
    buf = mmap.mmap(f.fileno(), 0)
    lines = 0
    readline = buf.readline
    while readline():
        lines += 1
    return lines

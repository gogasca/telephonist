import datetime
import random

import psycopg2

from api.version1_0.database import Db
from conf import settings
from utils import yaml_processor


def query_database(sqlquery):
    """

    :param sqlquery:
    :return:
    """
    try:
        db = Db.Db()
        db.initialize(dsn=settings.SQLALCHEMY_DSN)
        return db.query(sqlquery)

    except psycopg2.ProgrammingError, exception:
        print exception
    except Exception, exception:
        print exception


def failed_requests(response, filetype=1):
    """

    :param response:
    :param filetype:
    :return:
    """

    if filetype == 1:
        filename = settings.api_error_client
    if filetype == 2:
        filename = settings.api_error_recording_file
    if filetype == 3:
        filename = settings.api_error_speech_r
    if filetype == 4:
        filename = settings.download_error_file
    if filetype == 5:
        filename = settings.invalid_numbers

    if response:
        with open(filename, 'ab') as error_file:
            timestamp = '%s' % datetime.datetime.now()
            error_file.write(timestamp + ' ' + response)


def get_calling_number(number_list, country_code=None, smart_match=False, destination=None, **kwargs):
    """
        Find list of calling numbers based on country code
        Smart match:
            Extract called number prefix and find similarity with existing calling number
                If true Return local number
                Else return random number in our country list
        No smart match
            Return random number from list
        No list
            Return default

    :param number_list:
    :param smart_match:
    :param destination:
    :param kwargs:
    :return:
    """

    destination = country_code + destination

    if country_code in number_list:
        number_list = number_list[country_code]
    else:
        return settings.sip_callerid

    if number_list and smart_match is False:
        index = random.randint(0, len(number_list) - 1)
        return number_list[index]
    if number_list and smart_match and destination:
        destination_substr = destination[:5]
        for number in number_list:
            if destination_substr == number[:5]:
                return number
        index = random.randint(0, len(number_list) - 1)
        return number_list[index]

    return settings.sip_callerid


def get_cps_delay(cps=settings.cps):
    """

    :param cps:
    :return:
    """
    base_delay = 1000
    if settings.extra_delay:  # Add Extra 100ms to avoid Twilio CPS Error - 32001 SIP: Trunk CPS limit exceeded
        if settings.extra_delay_value:
            extra_delay = settings.extra_delay_value
        else:
            extra_delay = 100
        base_delay += extra_delay

    if cps != 0 and cps < 100 and cps <= settings.max_cps:
        delay = base_delay / cps
        return float(delay) / 1000
    else:
        # Return default 1 CPS
        return float(base_delay) / 1000


def update_database(sqlquery):
    """

    :rtype : object
    :param sqlquery:
    :return:
    """
    try:
        db = Db.Db()
        db.initialize(dsn=settings.SQLALCHEMY_DSN)
        db.update(sqlquery)

    except psycopg2.ProgrammingError, exception:
        print exception
    except Exception, exception:
        print exception


def get_hangup_reason_report(hangup_cause, fax_not_detected):
    """
    """
    try:
        if not fax_not_detected:  # Fax detected
            return 'FAX'

        wtn = yaml_processor.YamlProcessor(settings.hangup_reason_file).get_property('codes', 'wtn')
        nlis = yaml_processor.YamlProcessor(settings.hangup_reason_file).get_property('codes', 'nlis')
        if hangup_cause in wtn:
            return 'WTN'
        elif hangup_cause in nlis:
            return 'NLIS'
        else:
            return 'ERROR'

    except Exception, exception:
        print exception

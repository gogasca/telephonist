from conf import settings
from error import exceptions
from utils import FileReader


def check_call(json_request):
    """

    :param configuration:
    If provider is defined we are using non-default provider
    :return:
    """
    try:
        if json_request["provider"] == "sip":
            if all(k in json_request for k in ("to", "from", "description", "duration", "test")):
                return True
        else:
            if all(k in json_request for k in
                   ("to", "from", "url", "status_callback", "description", "duration", "test")):
                return True

        raise exceptions.InvalidCallRequest('Invalid Call Request')

    except Exception, e:
        print e


def check_campaign_country(country=settings.call_country_code):
    """

    :param country:
    :return:
    """
    if country == '+1':
        pass
    if country == '+55':
        pass
    if country == '+52':
        pass


def check_campaign(json_request):
    """

    :param json_request:
    :return:
    """
    try:

        if all(k in json_request for k in ("filename", "provider")):
            if json_request["provider"] in settings.valid_providers and \
                    FileReader.file_exists(json_request["filename"]):
                return True
        raise exceptions.InvalidCallRequest('Invalid Campaign Request')
    except Exception, e:
        print e

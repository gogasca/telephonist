# http://code.activestate.com/recipes/577423-convert-csv-to-xml/

import os

DELIMITER = ","  # "\t" "|" # delimiter used in the CSV file(s)


class Csv2Xml(object):
    def __init__(self, csvFile):
        self._csvFile = csvFile
        self._xmlFile = csvFile[:-4] + '.xml'

    def _initialize(self):
        """

        :return:
        """
        try:
            return os.path.isfile(self._csvFile)
        except IOError:
            return False

    def convert(self):
        """

        :return:
        """
        if not self._initialize():
            return

        csvFile = open(self._csvFile, 'rb')
        csvData = csvFile.readlines()
        csvFile.close()

        tags = csvData.pop(0).strip().replace(' ', '_').split(DELIMITER)
        xmlData = open(self._xmlFile, 'w')
        xmlData.write('<?xml version="1.0"?>' + "\n")
        # there must be only one top-level tag
        xmlData.write('<telephonist>' + "\n")
        for row in csvData:
            rowData = row.strip().split(DELIMITER)
            xmlData.write('<row>' + "\n")
            for i in range(len(tags)):
                xmlData.write('    ' + '<' + tags[i] + '>' \
                              + rowData[i] + '</' + tags[i] + '>' + "\n")
            xmlData.write('</row>' + "\n")
        xmlData.write('</telephonist>' + "\n")
        xmlData.close()

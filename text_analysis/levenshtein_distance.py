from nltk import stem, tokenize, edit_distance
from conf import settings


def levenshtein(text1, text2):
    """

    :param text1:
    :param text2:
    :return:
    """
    return edit_distance(text1, text2)


def normalize_levenshtein(text1, text2):
    """

    :param text1:
    :param text2: Fixed text
    :return:
    """
    levdist = edit_distance(text1, text2)
    length1 = len(text1)
    length2 = len(text2)
    lensum = length1 + length2
    if lensum != 0 and (lensum - levdist) > 0:
        nltkratio = ((lensum - levdist) / lensum)
        print "nltkratio %s" % nltkratio
        return nltkratio
    return 0


def normalize_porterstemmer(s):
    stemmer = stem.PorterStemmer()
    words = tokenize.wordpunct_tokenize(s.lower().strip())
    return ' '.join([stemmer.stem(w) for w in words])


def fuzzy_match(s1, s2, max_dist=settings.max_distance):
    return edit_distance(normalize_porterstemmer(s1),
                                 normalize_porterstemmer(s2)) <= max_dist


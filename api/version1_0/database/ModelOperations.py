import Model
from Model import db as db
from conf import settings


def insert_campaign(status, description, reference, start, request_data, campaign_type, test, filename, cps):
    """

    :param status:
    :param description:
    :param reference:
    :param start:
    :param request_data:
    :param campaign_type:
    :return:
    """
    try:
        campaign = Model.Campaign(status, description, reference, start, request_data, campaign_type, test, filename,
                                  cps)
        db.session.add(campaign)
        db.session.commit()
        return campaign.id
    except Exception, e:
        db.session.rollback()
        print e
    finally:
        db.session.close()


def insert_call(uuid, destination, request_data, campaign, description, test):
    """

    :param call_uuid:
    :param status:
    :param destination:
    :param request_data:
    :param campaign:
    :return:
    """
    try:
        # uuid, destination, request_data, campaign, start
        call = Model.Call(uuid, destination, request_data, campaign, "now()", description, test)
        db.session.add(call)
        db.session.commit()
        return call.id
    except Exception, e:
        db.session.rollback()
        print e
    finally:
        db.session.close()


def call_filter(request):
    """

    :param request:
    :return:
    """
    destination_value = request.args.get('destination')
    callsid_value = request.args.get('callsid')
    status_value = request.args.get('status')
    calls = None

    if destination_value and status_value:
        if status_value.isdigit():
            calls = Model.Call.query.filter(Model.Call.callsid == int(callsid_value),
                                            Model.Call.status == int(status_value)).limit(settings.items_per_page).all()
        return calls

    if callsid_value:
        calls = Model.Call.query.filter(Model.Call.callsid == int(callsid_value)).limit(settings.items_per_page).all()

    if destination_value:
        calls = Model.Call.query.filter(Model.Call.destination == int(destination_value)).limit(
            settings.items_per_page).all()

    if status_value:
        if status_value.isdigit():
            calls = Model.Call.query.filter(Model.Call.status == int(status_value)).limit(settings.items_per_page).all()

    return calls


def campaign_filter(request):
    """

    :param request:
    :return:
    """
    owner_id_value = request.args.get('owner_id')
    reference_value = request.args.get('reference')
    status_value = request.args.get('status')
    campaigns = None

    if owner_id_value and status_value:
        if status_value.isdigit():
            campaigns = Model.Campaign.query.filter(Model.Campaign.owner_id_value == int(owner_id_value),
                                                    Model.Campaign.status == int(status_value)).limit(
                settings.items_per_page).all()
        return campaigns

    if owner_id_value:
        campaigns = Model.Campaign.query.filter(Model.Campaign.callsid == int(owner_id_value)).limit(
            settings.items_per_page).all()

    if reference_value:
        campaigns = Model.Campaign.query.filter(Model.Campaign.reference == reference_value).limit(
            settings.items_per_page).all()

    if status_value:
        if status_value.isdigit():
            campaigns = Model.Campaign.query.filter(Model.Campaign.status == int(status_value)).limit(
                settings.items_per_page).all()

    return campaigns

import datetime
from collections import OrderedDict

from sqlalchemy import Boolean, Column, DateTime, Integer, String, text, Float
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.inspection import inspect

from utils import helper

Base = declarative_base()
metadata = Base.metadata
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


# sqlacodegen postgresql://telephonist:telephonist@159.203.222.147/telephonistdb > database/models.py

class Serializer(object):
    """
        Serialize information from database to a JSON Dictionary
    """

    def serialize(self):
        return {c: getattr(self, c) for c in inspect(self).attrs.keys()}

    @staticmethod
    def serialize_list(l):
        return [m.serialize() for m in l]


class AutoSerialize(object):
    """
        Mixin for retrieving public fields of model in json-compatible format'
    """
    __public__ = None

    def get_public(self, exclude=(), extra=()):
        "Returns model's PUBLIC data for jsonify"
        data = {}
        keys = self._sa_instance_state.attrs.items()
        public = self.__public__ + extra if self.__public__ else extra
        for k, field in keys:
            if public and k not in public: continue
            if k in exclude: continue
            value = self._serialize(field.value)
            if value:
                data[k] = value
        return data

    @classmethod
    def _serialize(cls, value, follow_fk=False):
        if type(value) in (datetime,):
            ret = value.isoformat()
        elif hasattr(value, '__iter__'):
            ret = []
            for v in value:
                ret.append(cls._serialize(v))
        elif AutoSerialize in value.__class__.__bases__:
            ret = value.get_public()
        else:
            ret = value

        return ret


class DictSerializable(object):
    """

    """

    def _asdict(self):
        result = OrderedDict()
        for key in self.__mapper__.c.keys():
            result[key] = getattr(self, key)
        return result


class ApiUser(Base):
    __tablename__ = 'api_user'

    id = Column(Integer, primary_key=True, server_default=text("nextval('api_user_id_seq'::regclass)"))
    username = Column(String(50))
    password = Column(String(250))
    created = Column(DateTime(True))

    def serialize(self):
        """

        :return:
        """
        d = Serializer.serialize(self)
        return d


class Call(db.Model, AutoSerialize, Serializer):
    __tablename__ = 'calls'

    id = Column(Integer, primary_key=True, server_default=text("nextval('calls_id_seq'::regclass)"))
    callsid = Column(String(128))
    recording_url = Column(String(256))
    recording_file = Column(String(256))
    recording_url = Column(String(256))
    recording_duration = Column(Integer)
    recording_text = Column(String(4096))
    call_start = Column(DateTime)
    call_end = Column(DateTime)
    status = Column(String(64))
    result = Column(Integer)
    destination = Column(String(256))
    campaign = Column(String(128))
    request_data = Column(String(4096))
    duration = Column(Integer)
    uuid = Column(String(128), nullable=False)
    description = Column(String(4096))
    analysis = Column(String(4096))
    hangup_cause = Column(String(128))
    hangup_cause_override = Column(String(128))
    hangup_cause_report = Column(String(128))
    is_test = Column(Boolean, server_default=text("false"))

    def serialize(self):
        """

        :return:
        """
        d = Serializer.serialize(self)
        return d

    def __init__(self, uuid, destination, request_data, campaign, start, description, test):
        """

        :param status:
        :param start:
        :param destination:
        :param campaign:
        :return:
        """
        self.uuid = uuid
        self.destination = destination
        self.request_data = request_data
        self.campaign = campaign
        self.call_start = start
        self.result = 0
        self.description = description
        self.is_test = test
        self.status = 'queued'


class Campaign(db.Model, AutoSerialize, Serializer):
    __tablename__ = 'campaign'

    id = Column(Integer, primary_key=True, server_default=text("nextval('campaign_id_seq'::regclass)"))
    description = Column(String(256))
    reference = Column(String(32), nullable=False)
    campaign_start = Column(DateTime)
    campaign_end = Column(DateTime)
    owner_id = Column(Integer)
    is_cancelled = Column(Boolean, server_default=text("false"))
    status = Column(Integer, server_default=text("0"))
    request_data = Column(String(4096))
    campaign_type = Column(String(256))
    report_created = Column(Boolean, server_default=text("false"))
    is_test = Column(Boolean, server_default=text("false"))
    filename = Column(String(256))
    cps = Column(Float)

    def serialize(self):
        """

        :return:
        """
        d = Serializer.serialize(self)
        campaign_info = self.campaign_status(self.reference)
        d['info'] = campaign_info
        del d['id']
        del d['is_cancelled']
        del d['owner_id']
        del d['request_data']
        del d['status']
        del d['report_created']
        return d

    def campaign_status(self, reference_value):
        """

        :param reference_value:
        :return:
        """
        campaign_details = dict()

        sqlquery = "SELECT COUNT(id) FROM calls WHERE calls.campaign = '" + \
                   reference_value + "' AND calls.status='queued'"
        queued_calls = helper.query_database(sqlquery)

        sqlquery = "SELECT COUNT(id) FROM calls WHERE calls.campaign = '" + \
                   reference_value + "'"
        processed_calls = helper.query_database(sqlquery)

        sqlquery = "SELECT COUNT(id) FROM calls WHERE calls.campaign = '" + \
                   reference_value + "' AND calls.call_start IS NOT NULL and calls.call_end IS NULL " \
                                     "AND calls.status!='queued'"
        active_calls = helper.query_database(sqlquery)

        sqlquery = "SELECT COUNT(id) FROM calls WHERE calls.campaign = '" + \
                   reference_value + "' AND calls.call_end IS NOT NULL"
        completed_calls = helper.query_database(sqlquery)

        sqlquery = "SELECT COUNT(id) FROM calls WHERE calls.campaign = '" + \
                   reference_value + "' AND sip_error LIKE '%CPS%'"
        cps_errors = helper.query_database(sqlquery)

        campaign_details["active_calls"] = int(active_calls)
        campaign_details["completed_calls"] = int(completed_calls)
        campaign_details["processed_calls"] = int(processed_calls)
        campaign_details["queued_calls"] = int(queued_calls)
        campaign_details["cps_errors"] = int(cps_errors)
        return campaign_details

    def __init__(self, status, description, reference, start, request_data, campaign_type, test, filename, cps):
        """

        :param status:
        :param description:
        :param reference:
        :param job_start:
        :param request_data:
        :param job_type:
        :return:
        """
        self.status = status
        self.description = description
        self.reference = reference
        self.campaign_start = start
        self.request_data = request_data
        self.campaign_type = campaign_type
        self.is_test = test
        self.filename = filename
        self.cps = cps

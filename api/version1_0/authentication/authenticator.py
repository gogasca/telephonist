__author__ = 'gogasca'

from conf import settings
from functools import wraps
from api.version1_0.database import Model
from flask import jsonify, request, g
from flask_httpauth import HTTPBasicAuth
auth = HTTPBasicAuth()


# =========================================================
# Flask Application
# =========================================================
@auth.verify_password
def verify_password(username_or_token, password):
    # first try to authenticate by token
    user = Model.ApiUser.verify_auth_token(username_or_token)
    if not user:
        # try to authenticate with username/password
        user = Model.ApiUser.query.filter_by(username=username_or_token).first()
        if not user or not user.verify_password(password):
            return False
    g.user = user
    return True


def check_auth(username, password):
    return username == settings.api_account and password == settings.api_password


def authenticate():
    message = {'message': "Authenticate."}
    resp = jsonify(message)
    resp.status_code = 401
    resp.headers['WWW-Authenticate'] = 'Basic realm="telephonist"'
    return resp


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth:
            return authenticate()

        elif not check_auth(auth.username, auth.password):
            return authenticate()
        return f(*args, **kwargs)

    return decorated

import logging
import platform
import sys

if platform.system() == 'Linux':
    sys.path.append('/usr/src/telephonist/')
else:
    sys.path.append('/Users/gogasca/Documents/OpenSource/Development/Python/telephonist/')
import warnings
warnings.filterwarnings("ignore")
from conf import settings, logger
from utils import banner

log = logger.LoggerManager().getLogger("__app__", logging_file=settings.api_logfile)
log.setLevel(level=logging.DEBUG)

# =========================================================
# Flask Application imports and database
# =========================================================
from flask import Flask
import flask_restful
from api.version1_0.database import Model
from api_main import ApiBase, Echo, Call, CallList, CallStatus, CallRecord, CallRecordStatus, CallRecordLocal, \
    Campaign, CampaignList, Status
from werkzeug.contrib.fixers import ProxyFix

# =========================================================
# Flask Main Application
# =========================================================

api_app = Flask(__name__)  # Flask Application
api_app.config.from_pyfile("../../../conf/settings.py")  # Flask configuration

telephonist_api = flask_restful.Api(api_app)  # Define API
# limiter = Limiter(api_app, key_func=get_remote_address, global_limits=["10 per second"])
db = Model.db.init_app(api_app)  # Initialize database

# =========================================================
# API Definition
# =========================================================

telephonist_api.add_resource(ApiBase, settings.api_base_url)
telephonist_api.add_resource(Echo, '/api/1.0/echo')
telephonist_api.add_resource(Call, '/api/1.0/call/<string:ref>')
telephonist_api.add_resource(CallList, '/api/1.0/call')
telephonist_api.add_resource(CallStatus, '/api/1.0/callstatus')
telephonist_api.add_resource(CallRecord, '/api/1.0/callrecord')
telephonist_api.add_resource(CallRecordStatus, '/api/1.0/recordstatus')
telephonist_api.add_resource(CallRecordLocal, '/api/1.0/record')
telephonist_api.add_resource(Campaign, '/api/1.0/campaign/<string:ref>')
telephonist_api.add_resource(CampaignList, '/api/1.0/campaign')
telephonist_api.add_resource(Status, '/api/1.0/status/<string:task_id>')

# =========================================================
# API Proxy WSGi for gunicorn
# =========================================================

api_app.wsgi_app = ProxyFix(api_app.wsgi_app)

# =========================================================
# API Logs
# =========================================================

log.info('Initializing Telephonist API >>>')
banner.horizontal(' Telephonist API v0.1 ')

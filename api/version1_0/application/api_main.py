import json
import logging
import time
import warnings

import flask_restful
import twilio.twiml

warnings.filterwarnings("ignore")

# =========================================================
# Telephonist imports
# =========================================================

from celery import Celery
from conf import settings, logger
from callcontrol import notify, CallInstance, CallRecording, CampaignD, Provider

# =========================================================
# Flask Application imports and database
# =========================================================

from api.version1_0.authentication import authenticator
from api.version1_0.controller import api_controller
from api.version1_0.database import Model, ModelOperations
from flask import current_app
from flask import jsonify, request, Response
from flask_restful import Resource
from sqlalchemy import desc

# =========================================================
# API Controller
# =========================================================

api = flask_restful.Api
log = logger.LoggerManager().getLogger("__app__", logging_file=settings.api_logfile)
log.setLevel(level=logging.DEBUG)


class ApiBase(Resource):
    @authenticator.requires_auth
    def get(self):
        """

        :return:
        """
        try:
            # =========================================================
            # GET API
            # =========================================================
            log.info(request.remote_addr + ' ' + request.__repr__())
            if request.headers['Content-Type'] == 'application/json':
                # =========================================================
                # Send API version information
                # =========================================================
                log.info('api() | GET | Version' + settings.api_version)
                response = json.dumps('version: ' + settings.api_version)
                resp = Response(response, status=200, mimetype='application/json')
                return resp

        except KeyError:
            response = json.dumps('Invalid type headers. Use application/json')
            resp = Response(response, status=415, mimetype='application/json')
            return resp

        except Exception, exception:
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error')
            resp = Response(response, status=500, mimetype='application/json')
            return resp


# =========================================================
# API Echo information
# =========================================================

class Echo(Resource):
    """Used for verifying API status"""

    #@authenticator.requires_auth
    def get(self):
        """

        :return:
        """
        log.info(request.remote_addr + ' ' + request.__repr__())
        log.info('api() | GET | Received request for Echo')
        response = json.dumps('Echo: GET. Parzee works')
        resp = Response(response, status=200, mimetype='application/json')
        return resp

    @authenticator.requires_auth
    def delete(self):
        log.info(request.remote_addr + ' ' + request.__repr__())
        log.info('api() | DELETE | Received request for Echo')
        response = json.dumps('Echo: DELETE. Parzee works')
        resp = Response(response, status=202, mimetype='application/json')
        return resp

    @authenticator.requires_auth
    def post(self):
        log.info(request.remote_addr + ' ' + request.__repr__())
        log.info('api() | POST | Received request for Echo')
        response = json.dumps('Echo: POST. Parzee works')
        resp = Response(response, status=202, mimetype='application/json')
        return resp

    @authenticator.requires_auth
    def put(self):
        log.info(request.remote_addr + ' ' + request.__repr__())
        log.info('api() | PUT | Received request for Echo')
        response = json.dumps('Echo: PUT. Parzee works')
        resp = Response(response, status=202, mimetype='application/json')
        return resp


class CampaignList(Resource):
    @api_controller.validate_json
    @api_controller.validate_schema('campaign')
    def post(self):
        """

        :return:
        """
        try:
            log.info(request.remote_addr + ' ' + request.__repr__())
            log.info('api() | POST | Received request for Campaign')
            start = time.time()
            # =========================================================
            # Launch celery Task. Initial dialout
            # =========================================================
            campaign_instance = CampaignD.CampaignD()
            campaign_information = api_controller.get_campaigninfo(request.json)

            campaign_instance.provider = Provider.Provider()
            campaign_instance.provider.name = campaign_information['provider']
            campaign_instance.provider.test = campaign_information['test']
            campaign_instance.provider.config_file = campaign_information['config_file']
            campaign_instance.test = campaign_instance.provider.test
            campaign_instance.filename = campaign_information['filename']
            campaign_instance.cps = campaign_information['cps']
            campaign_instance.country_code = campaign_information['country_code']
            campaign_instance.record = campaign_information['record']
            campaign_instance.transcribe = campaign_information['transcription']
            campaign_instance.blacklist = campaign_information['blacklist']
            campaign_instance.lookup = campaign_information['lookup']


            celery = Celery()
            celery.config_from_object("conf.celeryconfig")
            celery.send_task('process_campaign',
                             exchange='telephonist',
                             queue='bronze',
                             routing_key='telephonist.bronze',
                             kwargs={'campaign_instance': campaign_instance},
                             task_id=campaign_instance.reference,
                             retries=3)

            response = jsonify({'campaign': campaign_instance.reference})
            response.headers['Location'] = api.url_for(api(current_app),
                                                       Campaign,
                                                       ref=campaign_instance.reference,
                                                       _external=True,
                                                       _scheme=settings.api_scheme)
            end = time.time()
            log.info('api() | Campaign creation took: {} ms'.format(end - start))
            response.status_code = 202
            return response

        except Exception, exception:
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error. Please try again later')
            resp = Response(response, status=500, mimetype='application/json')
            return resp

        finally:
            # =========================================================
            # Insert campaign in database
            # =========================================================

            # status, description, reference, "now", request_data, campaign_type
            campaign_instance.id = ModelOperations.insert_campaign(0,
                                                                   "",
                                                                   campaign_instance.reference,
                                                                   "now",
                                                                   request.data,
                                                                   campaign_instance.provider.name,
                                                                   campaign_instance.provider.test,
                                                                   campaign_instance.filename,
                                                                   campaign_instance.cps)

    @authenticator.requires_auth
    def get(self):
        """

        :return:
        """
        try:

            # =========================================================
            # Get Campaigns
            # =========================================================

            log.info(request.remote_addr + ' ' + request.__repr__())

            if len(request.args) > 0:
                campaigns = ModelOperations.campaign_filter(request)
            else:

                # =========================================================
                # Limit the number of Jobs in request.
                # =========================================================

                campaigns = Model.Campaign.query.order_by(desc(Model.Campaign.campaign_start)).limit(
                    settings.items_per_page).all()

            response = []

            if campaigns:
                values = ['reference', 'campaign_start', 'campaign_end', 'status']
                response = [{value: getattr(d, value) for value in values} for d in campaigns]

            return jsonify(campaigns=response)

        except Exception, exception:
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error. Please try again later')
            resp = Response(response, status=500, mimetype='application/json')
            return resp


class Campaign(Resource):
    @authenticator.requires_auth
    def get(self, ref):
        """

        :param ref:
        :return:
        """

        try:

            # =========================================================
            # Get Campaign
            # =========================================================

            log.info(request.remote_addr + ' ' + request.__repr__())
            log.info('api() | Finding Campaign: ' + ref)

            campaign = Model.Campaign.query.filter(Model.Campaign.reference == ref).first()

            if campaign:
                log.info('api() | Campaign found: ' + ref)
                return jsonify(campaign.serialize())

            log.warn('api() | Campaign not found: ' + ref)
            resp = Response(status=404, mimetype='application/json')
            return resp

        except Exception, exception:
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error. Please try again later')
            resp = Response(response, status=500, mimetype='application/json')
            return resp


class CallList(Resource):
    @api_controller.validate_json
    @api_controller.validate_schema('call')
    def post(self):
        """
        :return:
        """
        try:
            log.info(request.remote_addr + ' ' + request.__repr__())
            log.info('api() | POST | Received request for Call')
            call_instance = CallInstance.Call()
            call_instance.provider = Provider.Provider()
            call_instance.call_info = api_controller.get_callinfo(request.json)
            call_instance.duration = call_instance.call_info["duration"]
            call_instance.country_code = call_instance.call_info["country_code"]
            call_instance.recording_enable = call_instance.call_info["record"]
            call_instance.provider.name = call_instance.call_info["provider"]
            call_instance.blacklist = call_instance.call_info["blacklist"]
            call_instance.provider.test = call_instance.call_info["test"]
            call_instance.test = call_instance.provider.test

            log.info("Call: {}| {} | Provider: {}| Info: {}".format(call_instance.uuid,
                                                                    call_instance.test,
                                                                    call_instance.provider.name,
                                                                    call_instance.call_info))

            # =========================================================
            # Launch celery Task. Initial dial
            # =========================================================
            celery = Celery()
            celery.config_from_object("conf.celeryconfig")
            celery.send_task('process_call',
                             exchange='telephonist',
                             queue='gold',
                             routing_key='telephonist.gold',
                             kwargs={'callinstance': call_instance},
                             task_id=call_instance.uuid,
                             retries=3)

            response = jsonify({'call': call_instance.uuid})
            response.headers['Location'] = api.url_for(api(current_app),
                                                       Call,
                                                       ref=call_instance.uuid,
                                                       _external=True,
                                                       _scheme=settings.api_scheme)
            response.status_code = 202
            return response
        except Exception, exception:
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error. Please try again later')
            resp = Response(response, status=500, mimetype='application/json')
            return resp
        finally:

            if call_instance:

                # =========================================================
                # Insert call in database #1
                # =========================================================

                call_instance.id = ModelOperations.insert_call(call_instance.uuid,
                                                               call_instance.call_info["to"],
                                                               request.data,
                                                               call_instance.call_info["campaign"],
                                                               call_instance.call_info["description"],
                                                               call_instance.call_info["test"])

    @authenticator.requires_auth
    def get(self):
        """

        :return:
        """
        try:

            # =========================================================
            # Get Campaigns
            # =========================================================

            log.info(request.remote_addr + ' ' + request.__repr__())

            if len(request.args) > 0:
                calls = ModelOperations.call_filter(request)
            else:

                # =========================================================
                # Limit the number of Jobs in request.
                # =========================================================

                calls = Model.Call.query.order_by(desc(Model.Call.start)).limit(settings.items_per_page).all()

            response = []

            if calls:
                values = ['uuid', 'callsid', 'start', 'destination', 'status']
                response = [{value: getattr(d, value) for value in values} for d in calls]

            return jsonify(calls=response)

        except Exception, exception:
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error. Please try again later')
            resp = Response(response, status=500, mimetype='application/json')
            return resp


class Call(Resource):
    @authenticator.requires_auth
    def get(self, uuid):
        try:
            log.info(request.remote_addr + ' ' + request.__repr__())

            # =========================================================
            # Get JOB by reference
            # =========================================================
            log.info('api() | Finding call: ' + uuid)
            call = Model.Call.query.filter(Model.Call.uuid == uuid).first()

            if call:
                log.info('api() | Call found: ' + call)
                return jsonify(call.serialize())

            log.warn('api() | Call not found: ' + uuid)
            resp = Response(status=404, mimetype='application/json')
            return resp

        except Exception, exception:
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error. Please try again later')
            resp = Response(response, status=500, mimetype='application/json')
            return resp


class CallStatus(Resource):
    def post(self):
        try:
            log.info(request.remote_addr + ' ' + request.__repr__())
            log.info('api() | POST | Call Status')
            # =========================================================
            # Get Twilio call status information
            # =========================================================

            call_sid = request.form['CallSid']
            call_duration = request.form['CallDuration']
            call_status = request.form['CallStatus']

            # Update call record

            notify.update_call_status(callsid=call_sid, status=call_status, duration=call_duration)
            log.info('api() | POST | Call Status: {} {} {}'.format(call_sid, call_duration, call_status))
            resp = Response(status=200)
            return resp

        except Exception, exception:
            log.exception(exception.__repr__())
            resp = Response(status=500)
            return resp

    @authenticator.requires_auth
    def get(self, uuid):
        """

        :param uuid:
        :return:
        """
        try:
            # =========================================================
            # GET API
            # =========================================================
            log.info(request.remote_addr + ' ' + request.__repr__())

            # =========================================================
            # Send API version information
            # =========================================================
            log.info('api() | GET | Call Status' + uuid)

            call_instance = Model.Call.query.filter(Model.Call.uuid == uuid).first()
            if call_instance:
                log.info('api() | Call found: ' + uuid)
                return jsonify(call_instance.serialize())

            log.warn('api() | Call not found: ' + uuid)
            resp = Response(status=404, mimetype='application/json')
            return resp

        except KeyError:
            response = json.dumps('Invalid type headers. Use application/json')
            resp = Response(response, status=415)
            return resp

        except Exception, exception:
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error')
            resp = Response(response, status=500)
            return resp


class CallRecordLocal(Resource):
    @api_controller.validate_json
    def post(self):
        """

        :return:
        """
        try:
            # =========================================================
            # GET CallRecord
            # =========================================================

            log.info(request.remote_addr + ' ' + request.__repr__())
            callrecord_information = api_controller.get_callrecordlocal(request.json)

            callrecord_instance = CallRecording.CallRecording()
            callrecord_instance.uuid = callrecord_information['Uuid']
            callrecord_instance.country_code = callrecord_information['CountryCode']
            callrecord_instance.recording_url = callrecord_information['RecordingUrl']
            callrecord_instance.recording_file = callrecord_information['RecordingFile']
            callrecord_instance.recording_type = "sip"
            celery = Celery()
            celery.config_from_object("conf.celeryconfig")
            celery.send_task('process_recording',
                             exchange='telephonist',
                             queue='silver',
                             routing_key='telephonist.silver',
                             kwargs={'callrecord_instance': callrecord_instance},
                             task_id='REC' + callrecord_instance.uuid,
                             retries=3)

            response = jsonify({'call': callrecord_instance.uuid})
            response.headers['Location'] = api.url_for(api(current_app),
                                                       Call,
                                                       ref=callrecord_instance.uuid,
                                                       _external=True,
                                                       _scheme=settings.api_scheme)
            response.status_code = 202
            return response

        except Exception, exception:
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error')
            resp = Response(response, status=500)
            return resp


class CallRecord(Resource):
    def post(self):
        """

        :return:
        """
        try:
            # =========================================================
            # GET CallRecord
            # =========================================================
            log.info(request.remote_addr + ' ' + request.__repr__())
            response = twilio.twiml.Response()
            callinfo = api_controller.get_twiliocallinfo(request)
            log.info('api() | POST | CallRecord {} {}'.format(callinfo['to'], callinfo['callsid']))

            # =========================================================
            # GET CallRecord
            # =========================================================

            response.record(maxLength=settings.record_duration,
                            playBeep=settings.record_beep,
                            trim="do-not-trim",
                            timeout=settings.record_silence_duration,
                            action="http://73ee2e07.ngrok.io/api/1.0/recordstatus")
            log.info('api() | Response: {}'.format(response))
            if isinstance(response, twilio.twiml.Response):
                return Response(str(response), mimetype='text/xml')
            else:
                response = json.dumps('Internal Server Error')
                resp = Response(response, status=500)
                return resp

        except Exception, exception:
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error')
            resp = Response(response, status=500)
            return resp


class CallRecordStatus(Resource):
    def post(self):
        """

        :return:
        """
        try:
            # =========================================================
            # POST CallRecordStatus
            # =========================================================
            log.info(request.remote_addr + ' ' + request.__repr__())
            log.info('api() | POST | CallRecordStatus')

            response = twilio.twiml.Response()
            callrecord_instance = CallRecording.CallRecording()
            callrecord_instance.callsid = request.form['CallSid']
            callrecord_instance.recording_url = request.form['RecordingUrl']
            callrecord_instance.recording_duration = request.form["RecordingDuration"]
            call_status = request.form['CallStatus']
            if 'Digits' in request.args:
                digits = request.form['Digits']
            else:
                digits = ''

            log.info('api() | POST | CallRecordStatus {} {} {} {} {}'.format(callrecord_instance.callsid,
                                                                             callrecord_instance.recording_url,
                                                                             callrecord_instance.recording_duration,
                                                                             digits, call_status))
            celery = Celery()
            celery.config_from_object("conf.celeryconfig")
            celery.send_task('process_recording',
                             exchange='telephonist',
                             queue='silver',
                             routing_key='telephonist.silver',
                             kwargs={'callrecord_instance': callrecord_instance},
                             task_id='REC' + callrecord_instance.callsid,
                             retries=3)

            if isinstance(response, twilio.twiml.Response):
                return Response(str(response), mimetype='text/xml')
            else:
                response = json.dumps('Internal Server Error')
                resp = Response(response, status=500)
                return resp

        except Exception, exception:
            log.exception(exception.__repr__())
            resp = Response('Internal Server Error', status=500)
            return resp


class Status(Resource):
    @authenticator.requires_auth
    def get(self, task_id):
        try:
            log.info(request.remote_addr + ' ' + request.__repr__())

            log.info('api() | Creating task: ' + task_id)
            start = time.time()
            celery = Celery()
            celery.config_from_object('conf.celeryconfig')
            log.info('api() | Status received request for Task: ' + str(task_id))
            task = celery.AsyncResult(task_id)

            # =========================================================
            # Verify task exists and match
            # =========================================================

            if task and len(task_id) == 10:
                log.info(task.state)

                if task.state == 'PENDING':
                    # job did not start yet
                    response = {
                        'state' : task.state,
                        'status': 'Task is in progress...'

                    }
                elif task.state != 'FAILURE':
                    response = {
                        'state' : task.state,
                        'status': str(task.info),
                    }
                    if task.info:
                        if 'result' in task.info:
                            response['result'] = task.info['result']

                else:
                    response = {
                        'state' : task.state,
                        'status': str(task.info),  # this is the exception raised
                    }

                end = time.time()
                log.info('api() | Task creation took: {} ms'.format(end - start))
                return jsonify(response)

            else:
                resp = Response(status=404, mimetype='application/json')
                return resp

        except Exception, exception:
            log.exception(exception.__repr__())
            response = json.dumps('Internal Server Error. Please try again later')
            resp = Response(response, status=500, mimetype='application/json')
            return resp

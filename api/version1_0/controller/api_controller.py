import numbers
from functools import wraps

from flask import (
    request,
    abort
)

from conf import settings
from utils import validator


# =========================================================
# Validate Request comes in valid JSON form
# =========================================================

def validate_json(f):
    @wraps(f)
    def wrapper(*args, **kw):
        try:
            request.json
        except Exception:
            msg = "Request must be a valid JSON. Use Content-Type: application/json"
            abort(400, description=msg)
        return f(*args, **kw)

    return wrapper


def validate_schema(schema_name):
    """

    :param schema_name:
    :return:
    """

    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kw):
            try:
                if schema_name == 'call':
                    # =========================================================
                    # Validate POST for new call
                    # =========================================================
                    validator.check_call(request.json)
                if schema_name == 'campaign':
                    # =========================================================
                    # Validate POST for new campaign
                    # =========================================================
                    validator.check_campaign(request.json)

            except Exception, exception:
                print exception
                msg = "Request must be a valid JSON for " + str(schema_name)
                abort(400, description=msg)
            return f(*args, **kw)

        return wrapper

    return decorator


def get_twiliocallinfo(request):
    """

    :param json_request:
    :return:
    """
    try:
        if request:
            call_info = dict()
            call_info['to'] = request.form['Called']
            call_info['country'] = request.form['CallerCountry']
            call_info['callsid'] = request.form['CallSid']
            return call_info
        else:
            raise ValueError
    except TypeError, e:
        print e


def get_campaigninfo(json_request):
    """

    :param json_request:
    :return:
    """
    if json_request:
        campaign_info = dict()
        campaign_info["filename"] = json_request["filename"]
        campaign_info["provider"] = json_request["provider"]
        # Test Campaign
        if "test" in json_request:
            campaign_info["test"] = json_request["test"]
        else:
            campaign_info["test"] = False

        # Provider configuration
        if "config_file" in json_request:
            campaign_info["config_file"] = json_request["config_file"]
        else:
            campaign_info["config_file"] = settings.filepath + settings.provider_folder + campaign_info[
                "provider"] + '.yaml'

        # CPS
        if "cps" in json_request:
            if isinstance(json_request["cps"], numbers.Number):
                campaign_info["cps"] = json_request["cps"]
            else:
                campaign_info["cps"] = None
        else:
            campaign_info["cps"] = None

        # Country code
        if "country_code" in json_request:
            campaign_info["country_code"] = str(json_request["country_code"])
        else:
            campaign_info["country_code"] = None

        # Record
        if "record" in json_request:
            campaign_info["record"] = json_request["record"]
        else:
            campaign_info["record"] = False

        # Transcription
        if "transcription" in json_request:
            campaign_info["transcription"] = json_request["transcription"]
        else:
            campaign_info["transcription"] = False

        # Blacklist enabled
        if "blacklist" in json_request:
            campaign_info["blacklist"] = json_request["blacklist"]
        else:
            campaign_info["blacklist"] = False

        if "lookup" in json_request:
            campaign_info["lookup"] = json_request["lookup"]
        else:
            campaign_info["lookup"] = False

        return campaign_info

    else:
        raise ValueError


def get_callrecordlocal(json_request):
    """

    :param json_request:
    :return:
    """
    if json_request:
        callrecordinfo = dict()
        callrecordinfo["Uuid"] = json_request["Uuid"]
        callrecordinfo["RecordingFile"] = json_request["RecordingFile"]
        if "RecordingUrl" in json_request:
            callrecordinfo["RecordingUrl"] = json_request["RecordingUrl"]
        else:
            callrecordinfo["RecordingUrl"] = None
        if "CountryCode" in json_request:
            callrecordinfo["CountryCode"] = json_request["CountryCode"]
        else:
            callrecordinfo["CountryCode"] = None
        return callrecordinfo

    else:
        raise ValueError


def get_callinfo(json_request):
    """

    :param json_request:
    :return:
    """

    if json_request:
        call_info = dict()
        if "provider" in json_request:
            if json_request["provider"] == "sip":
                call_info["campaign"] = json_request["campaign"]
                call_info["description"] = json_request["description"]
                call_info["duration"] = json_request["duration"]
                call_info["from"] = json_request["from"]
                call_info["provider"] = json_request["provider"]
                call_info["to"] = json_request["to"]
                call_info["test"] = json_request["test"]

                if "country_code" in json_request:
                    call_info["country_code"] = str(json_request["country_code"])
                else:
                    call_info["country_code"] = None

                if "record" in json_request:
                    call_info["record"] = json_request["record"]
                else:
                    call_info["record"] = False

                if "blacklist" in json_request:
                    call_info["blacklist"] = json_request["blacklist"]
                else:
                    call_info["blacklist"] = False

            if json_request["provider"] == "twilio":
                call_info["campaign"] = json_request["campaign"]
                call_info["description"] = json_request["description"]
                call_info["duration"] = json_request["duration"]
                call_info["from"] = json_request["from"]
                call_info["provider"] = json_request["provider"]
                call_info["to"] = json_request["to"]
                call_info["status_callback"] = json_request["status_callback"]
                call_info["url"] = json_request["url"]
                call_info["test"] = json_request["test"]

                if "country_code" in json_request:
                    call_info["country_code"] = json_request["country_code"]
                else:
                    call_info["country_code"] = None

                if "blacklist" in json_request:
                    call_info["blacklist"] = json_request["blacklist"]
                else:
                    call_info["blacklist"] = False

        return call_info
    else:
        raise ValueError

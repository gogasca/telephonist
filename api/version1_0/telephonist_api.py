__author__ = 'gogasca'

# =========================================================
# main application | gunicorn telephonist_api:api_app --bind 0.0.0.0:8081
# =========================================================

from application.app import api_app

if __name__ == "__main__":
    api_app.run(debug=False, threaded=True)

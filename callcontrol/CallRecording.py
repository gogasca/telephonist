import logging
import time
import urllib2

import CallInstance
import notify
from conf import logger
from conf import settings
from utils import helper

log = logger.LoggerManager().getLogger("__app__", logging_file=settings.api_logfile)
log.setLevel(level=logging.DEBUG)


class CallRecording(CallInstance.Call):
    """
    CallRecording instance
    """

    def __init__(self):
        self._callsid = None
        self._recording_url = None
        self._recording_duration = 0
        self._recording_file = None
        self._recording_type = settings.default_provider
        self.country_code = '1'  # Default English
        self.is_download_completed = False

    @property
    def recording_type(self):
        return self._recording_type

    @recording_type.setter
    def recording_type(self, recording_type):
        self._recording_type = recording_type

    @property
    def recording_url(self):
        return self._recording_url

    @recording_url.setter
    def recording_url(self, recording_url):
        self._recording_url = recording_url

    @property
    def recording_duration(self):
        return self._recording_duration

    @recording_duration.setter
    def recording_duration(self, recording_duration):
        self._recording_duration = recording_duration

    @property
    def recording_file(self):
        return self._recording_file

    @recording_file.setter
    def recording_file(self, recording_file):
        self._recording_file = recording_file
        if self.callsid:
            notify.call_recording_details(self.callsid, recording_file)

    def update_database_record(self):

        if notify.call_recording_url(self.callsid, self.recording_url, self.recording_duration):
            log.info(self.callsid, self.recording_url, self.recording_duration)
            return True
        else:
            log.error('CallRecording() Database update failed')
            return False

    def download_record_file(self):

        for n in range(0, 3):
            try:
                if self.recording_url:
                    log.info('CallRecording() URL: {}'.format(self.recording_url))
                    self.recording_file = settings.record_local_directory + self.recording_url.split('/')[-1]
                    #log.info(self.recording_file)
                    start = time.time()
                    req = urllib2.Request(self.recording_url, headers={'User-Agent' : 'Mozilla/5.0'})
                    u = urllib2.urlopen(req)
                    f = open(self.recording_file, 'wb')
                    meta = u.info()
                    file_size = int(meta.getheaders("Content-Length")[0])
                    #log.info("CallRecording() Downloading: %s Bytes: %s" % (self.recording_file, file_size))
                    file_size_dl = 0
                    block_sz = 8192
                    while True:
                        buffer = u.read(block_sz)
                        if not buffer:
                            break

                        file_size_dl += len(buffer)
                        f.write(buffer)
                        status = r"%10d  [%3.2f%%]" % (file_size_dl, file_size_dl * 100. / file_size)
                        status += chr(8) * (len(status) + 1)

                    end = time.time()
                    log.info("CallRecording() {} Download took: {} secs ".format(self.recording_file, end - start))
                    f.close()
                    self.is_download_completed = True
                    if self.uuid:
                        log.info("CallRecording() UUID: {}".format(self.uuid))
                        notify.call_recording_download_completed(uuid=self.uuid)
                    if self.callsid:
                        log.info("CallRecording() call_sid: {}".format(self.callsid))
                        notify.call_recording_download_completed(callsid=self.callsid)
                    return
                else:
                    log.warn('CallRecording() No recording file defined')
                    break

            except urllib2.HTTPError, exception:
                log.exception(exception)
            except urllib2.URLError, exception:
                log.exception(exception)
            except Exception, exception:
                log.exception(exception)

        if self.recording_url:
            result = self.recording_url
        else:
            result = "Undefined self.recording_url"
        log.error('CallRecording() Request never succeeded'.format(result))
        helper.failed_requests(result + '/n', 4)
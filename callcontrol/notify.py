import json
import re

from utils import helper


def call_init(uuid=None, sid=None, **kwargs):
    """

    :param call_uuid:
    :param callsid:
    :return:
    """
    if sid and uuid:
        sqlquery = """UPDATE calls SET callsid='""" + sid + """' WHERE calls.uuid='""" + uuid + "'"
        helper.update_database(sqlquery)
        return
    if uuid:
        sqlquery = """UPDATE calls SET status='initialized' WHERE uuid='""" + uuid + "'"
        helper.update_database(sqlquery)
        return


def call_result(call_sid=None, call_uuid=None, result=0, **kwargs):
    """

    :param call_sid:
    :param call_uuid:
    :param result:
    :return:
    """
    sqlquery = """UPDATE calls SET result=""" + str(result)

    if call_sid:
        sqlquery += """ WHERE calls.callsid='""" + call_sid + "'"

    if call_uuid:
        sqlquery += """ WHERE calls.uuid='""" + call_uuid + "'"

    helper.update_database(sqlquery)


def call_end(callsid=None, call_uuid=None, result=0, duration=0, status="", hangup_cause=None, summary=""):
    """

    :param callsid:
    :param call_uuid:
    :param result:
    :param duration:
    :param status:
    :param hangup_cause:
    :param summary:
    :return:
    """

    fax_not_detected = True
    sqlquery = """UPDATE calls SET call_end='now()',result=""" + str(result)

    if status:
        sqlquery += ",status='" + status + "'"

    if summary:
        sqlquery += ",summary='" + summary + "'"
        fax_not_detected = re.search(r'Fax:\s0', summary)

    if hangup_cause:
        if hangup_cause == 'BLACKLIST':
            sqlquery += ",hangup_cause='" + hangup_cause + "',is_blacklisted=true"
        else:
            sqlquery += ",hangup_cause='" + hangup_cause + "'"

        hangup_cause_report = helper.get_hangup_reason_report(hangup_cause, fax_not_detected)
        if hangup_cause_report:
            sqlquery += ",hangup_cause_report='" + hangup_cause_report + "'"

    if callsid and result:
        if duration:
            sqlquery += """,duration=""" + str(duration) + """ WHERE calls.callsid='""" + callsid + "'"
        else:
            sqlquery += """  WHERE calls.callsid='""" + callsid + "'"

        helper.update_database(sqlquery)
        return True

    if call_uuid and result:
        if duration:
            sqlquery += """,duration=""" + str(duration) + """ WHERE calls.uuid='""" + call_uuid + "'"
        else:
            sqlquery += """ WHERE calls.uuid='""" + call_uuid + "'"

        helper.update_database(sqlquery)
        return True


def call_update_callid(uuid, twilio_callsid, sip_callid):
    """

    :param uuid:
    :param twilio_callsid:
    :return:
    """

    sqlquery = """UPDATE calls SET """

    if twilio_callsid:
        sqlquery += "callsid='" + twilio_callsid + "'"

    if sip_callid:
        if twilio_callsid:
            sqlquery += ","
        sqlquery += "sip_callid='" + sip_callid + "'"

    if uuid and (twilio_callsid or sip_callid):
        sqlquery += """ WHERE calls.uuid='""" + uuid + "'"
        helper.update_database(sqlquery)
        return True


def call_media_detected(uuid, status):
    """

    :param uuid:
    :return:
    """
    if uuid:
        sqlquery = """UPDATE calls SET media_detected=true, status='""" + status + """' WHERE calls.uuid='""" + uuid + "'"
        helper.update_database(sqlquery)
        return True


def call_sip_error(uuid, sip_error):
    """

    :param uuid:
    :param sip_error:
    :return:
    """
    if uuid and sip_error:
        sqlquery = """UPDATE calls SET sip_error='""" + str(sip_error) + """' WHERE calls.uuid='""" + uuid + "'"
        helper.update_database(sqlquery)
        return True


def call_fax(uuid):
    """

    :param uuid:
    :return:
    """
    if uuid:
        sqlquery = """UPDATE calls SET is_fax=true WHERE calls.uuid='""" + uuid + "'"
        helper.update_database(sqlquery)
        return True


def call_recording_download_completed(callsid=None, uuid=None, **kwargs):
    """

    :param callsid:
    :param uuid:
    :param kwargs:
    :return:
    """
    if uuid:
        sqlquery = """UPDATE calls SET recording_download=true WHERE calls.uuid='""" + uuid + "'"
        helper.update_database(sqlquery)
        return True

    if callsid:
        sqlquery = """UPDATE calls SET recording_download=true WHERE calls.callsid='""" + callsid + "'"
        helper.update_database(sqlquery)
        return True


def call_recording_details(callsid=None, uuid=None, recording_file=None, recording_url=None, **kwargs):
    """

    :param callsid:
    :param uuid:
    :param recording_file:
    :param recording_url:
    :param kwargs:
    :return:
    """

    if uuid and recording_file and recording_url:
        sqlquery = """UPDATE calls SET recording_file='""" + recording_file + """', recording_url='""" + \
                   recording_url + """' WHERE calls.uuid='""" + uuid + "'"
        helper.update_database(sqlquery)
        return True

    if uuid and recording_file:
        sqlquery = """UPDATE calls SET recording_file='""" + recording_file + """' WHERE calls.uuid='""" + uuid + "'"
        helper.update_database(sqlquery)
        return True

    if callsid and recording_file:
        sqlquery = """UPDATE calls SET recording_file='""" + recording_file + """' WHERE calls.callsid='""" + callsid + "'"
        helper.update_database(sqlquery)
        return True


def call_recording_text(callsid, uuid, text, **kwargs):
    """

    :param callsid:
    :param uuid:
    :param text:
    :param kwargs:
    :return:
    """
    if callsid and text:
        sqlquery = """UPDATE calls SET recording_text='""" + text + """' WHERE calls.callsid='""" + callsid + "'"
        helper.update_database(sqlquery)
        return True
    if uuid and text:
        sqlquery = """UPDATE calls SET recording_text='""" + text + """' WHERE calls.uuid='""" + uuid + "'"
        helper.update_database(sqlquery)
        return True


def call_recording_analysis(callsid, uuid, analysis, **kwargs):
    """

    :param callsid:
    :param uuid:
    :param analysis:
    :param kwargs:
    :return:
    """
    if callsid and analysis:
        analysis = json.dumps(analysis)
        sqlquery = """UPDATE calls SET analysis='""" + analysis + """' WHERE calls.callsid='""" + callsid + "'"
        helper.update_database(sqlquery)
        return True
    if uuid and analysis:
        analysis = json.dumps(analysis)
        sqlquery = """UPDATE calls SET analysis='""" + analysis + """' WHERE calls.uuid='""" + callsid + "'"
        helper.update_database(sqlquery)
        return True


def call_recording_url(callsid, uuid, recording_url, recording_duration):
    """

    :param callsid:
    :param recording_url:
    :param recording_duration:
    :return:
    """
    if callsid and recording_url and recording_duration:
        sqlquery = """UPDATE calls SET recording_url='""" + recording_url + """', recording_duration=""" + \
                   recording_duration + """ WHERE calls.callsid='""" + callsid + "'"
        helper.update_database(sqlquery)
        return True

    if uuid and recording_url:
        sqlquery = """UPDATE calls SET recording_url='""" + recording_url + """' WHERE calls.uuid='""" + uuid + "'"
        helper.update_database(sqlquery)
        return True


def update_hangup_cause_report(uuid, reason):
    """

    :param uuid:
    :param reason:
    :return:
    """
    if uuid and reason:
        sqlquery = """UPDATE calls SET hangup_cause_report='""" + reason + """' WHERE calls.uuid='""" + uuid + "'"
        helper.update_database(sqlquery)


def update_call_status(uuid, status, duration=None):
    """

    :param callsid:
    :param status:
    :param duration:
    :return:
    """
    if uuid and status and duration:
        sqlquery = """UPDATE calls SET status='""" + status + """', duration=""" + str(duration) \
                   + """ WHERE calls.uuid='""" + uuid + "'"
        helper.update_database(sqlquery)

    if uuid and status:
        sqlquery = """UPDATE calls SET status='""" + status + """' WHERE calls.uuid='""" + uuid + "'"
        helper.update_database(sqlquery)

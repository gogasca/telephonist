class Provider(object):
    def __init__(self):
        self._name = None
        self._host = None
        self._port = None
        self._account_name = None
        self._config_file = None
        self._token = None
        self._password = None
        self._gateway = None
        self._test = False
        self._blacklist = False
        self._blacklist_file = None

    @property
    def config_file(self):
        return self._config_file

    @config_file.setter
    def config_file(self, config_file):
        self._config_file = config_file

    @property
    def blacklist(self):
        return self._blacklist

    @blacklist.setter
    def blacklist(self, blacklist):
        self._blacklist = blacklist

    @property
    def blacklist_file(self):
        return self._blacklist_file

    @blacklist_file.setter
    def blacklist_file(self, blacklist_file):
        self._blacklist_file = blacklist_file


    @property
    def name(self):
        """

        :return:
        """
        return self._name

    @name.setter
    def name(self, name):
        """

        :param name:
        :return:
        """
        self._name = name

    @property
    def test(self):
        return self._test

    @test.setter
    def test(self, test):
        self._test = test

    @property
    def gateway(self):
        """

        :return:
        """
        return self._gateway

    @gateway.setter
    def gateway(self, gateway):
        """

        :param gateway:
        :return:
        """
        self._gateway = gateway

    @property
    def host(self):
        """

        :return:
        """
        return self._host

    @host.setter
    def host(self, host):
        """

        :param hostname:
        :return:
        """
        self._host = host

    @property
    def port(self):
        """

        :return:
        """
        return self._port

    @port.setter
    def port(self, port):
        """

        :param hostname:
        :return:
        """
        self._port = port

    @property
    def account_name(self):
        """

        :return:
        """
        return self._account_name

    @account_name.setter
    def account_name(self, account_name):
        """

        :param provider:
        :return:
        """
        self._account_name = account_name

    @property
    def token(self):
        """

        :return:
        """
        return self._token

    @token.setter
    def token(self, token):
        """

        :param provider:
        :return:
        """
        self._token = token

    @property
    def password(self):
        """

        :return:
        """
        return self._password

    @password.setter
    def password(self, password):
        """

        :param password:
        :return:
        """
        self._password = password

    def __str__(self):
        return '{} {}'.format(self.name, self.test)

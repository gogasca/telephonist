import logging

from twilio import TwilioRestException

from callcontrol import notify
from conf import logger
from conf import settings
from utils import Generator

log = logger.LoggerManager().getLogger("__app__")
log.setLevel(level=logging.DEBUG)


class Call(object):
    def __init__(self):
        self._blacklist = False
        self._call_info = dict()
        self._callsid = None
        self._country_code = settings.call_country_code
        self._client = None
        self._description = None
        self._duration = 18
        self._information = None
        self._provider = None
        self._recording_enable = False
        self._recording = None
        self._test = None
        self._transcribe = False
        self._uuid = Generator.Generator().get_uuid()
        
    @property
    def uuid(self):
        return self._uuid

    @uuid.setter
    def uuid(self, uuid):
        self._uuid = uuid

    @property
    def recording_enable(self):
        return self._recording_enable

    @recording_enable.setter
    def recording_enable(self, recording_enable):
        self._recording_enable = recording_enable

    @property
    def test(self):
        return self._test

    @test.setter
    def test(self, test):
        self._test = test

    @property
    def country_code(self):
        return self._country_code

    @country_code.setter
    def country_code(self,country_code):
        self._country_code = country_code

    @property
    def description(self):
        return self._description

    @description.setter
    def description(self, description):
        self._description = description

    @property
    def information(self):
        return self._information

    @information.setter
    def information(self, information):
        self._information = information

    @property
    def provider(self):
        return self._provider

    @provider.setter
    def provider(self, provider):
        self._provider = provider

    @property
    def client(self):
        return self._client

    @client.setter
    def client(self, client):
        self._client = client

    @property
    def duration(self):
        return self._duration

    @duration.setter
    def duration(self, duration):
        self._duration = duration

    @property
    def callsid(self):
        return self._callsid

    @callsid.setter
    def callsid(self, callsid):
        self._callsid = callsid

    @property
    def call_info(self):
        return self._call_info

    @call_info.setter
    def call_info(self, call_info):
        self._call_info = call_info

    @property
    def recording(self):
        return self._recording

    @recording.setter
    def recording(self, recording):
        self._recording = recording

    @property
    def transcribe(self):
        return self._transcribe

    @transcribe.setter
    def transcribe(self, transcribe):
        self._transcribe = transcribe

    @property
    def blacklist(self):
        return self._blacklist

    @blacklist.setter
    def blacklist(self, blacklist):
        self._blacklist = blacklist

    def status(self):
        """

        :return:
        """
        try:
            log.info("status() Getting call status for: {}".format(self.callsid))
            if self.client:
                log.info('status() Contacting Twilio: {}'.format(self.callsid))
                call = self._client.calls.get(self.callsid)
                if call:
                    log.info('status() Call Status: {}|{}|{}'.format(self.uuid, self.callsid, call.status))
                    return call.status
                else:
                    log.error('status() Unable to get call status')

        except Exception, e:
            log.exception(e)

    def dial(self):
        """
        Place call using Twilio Libraries
        :return:
        """
        try:
            log.info("dial() {}".format(self.call_info))
            if self.client:
                log.info('dial() Creating call via API...')
                call = self.client.calls.create(to=self.call_info["to"],
                                                from_=self.call_info["from"],
                                                url=self.call_info["url"],
                                                status_callback=self.call_info["status_callback"],
                                                status_callback_method="POST",
                                                timeout=self.call_info["duration"])

                log.info('dial() New Call CallSid: {}'.format(call.sid))
                self.callsid = call.sid
                return call
            else:
                log.error('dial() Client not defined.')
        except TwilioRestException, exception:
            log.info('Exception found in Twilio API {}'.format(exception))
        except Exception, exception:
            log.exception(exception)

    def hangup(self, update_db=True, result=1):
        """

        :return:
        """
        if self.callsid and self.client:
            self.client.calls.hangup(self.callsid)
            log.info('hangup() Call ended: {}'.format(self.callsid))
            if update_db:
                notify.call_end(self.callsid, result)
            return True
        else:
            log.error('hangup() Unable to end call'.format(self.callsid))
            return False

import logging

from celery.exceptions import SoftTimeLimitExceeded

from callcontrol import notify
from callcontrol.modules.apps import handle_calls
from callcontrol.modules.freeswitch import Client
from callcontrol.modules.freeswitch import FreeswitchCall
from callcontrol.modules.twiliomod import TwilioCall
from conf import logger
from conf import settings
from error.exceptions import ESLError

log = logger.LoggerManager().getLogger("__app__")
log.setLevel(level=logging.DEBUG)


class CallProcessor(object):
    def __init__(self):
        self._call = None

    @property
    def call(self):
        return self._call

    @call.setter
    def call(self, call):
        self._call = call

    def blacklist_call(self):
        try:
            log.info('blacklist_call() CALL_BLACKLIST | CALL {}'.format(self.call.uuid))
            notify.call_end(call_uuid=self.call.uuid, result=-1, status='blacklist', hangup_cause='BLACKLIST')
            return True
        except Exception, exception:
            log.exception(exception)

    def freeswitch_call(self):
        """

        :return:
        """
        try:
            client = Client.Client(host=self.call.provider.host)
            client.connect()
            if client.connected():
                freeswitch_call_instance = FreeswitchCall.FreeSwitchCall(connection=client.con)
                freeswitch_call_instance.call = self.call
                freeswitch_params = "ignore_early_media=false," \
                                    "bridge_early_media=false," \
                                    "originate_timeout=70," \
                                    "bridge_answer_timeout=70," \
                                    "call_timeout=70," \
                                    "[leg_timeout=70]," \
                                    "execute_on_answer_1=\'sched_hangup " + \
                                    str(self.call.duration) + " " + settings.sip_hangup_reason + "\'"

                # Dial call using ESL Freeswitch API
                freeswitch_call_instance.dial(record=self.call.recording_enable,
                                              params=freeswitch_params,
                                              app=handle_calls.HandleCalls())
                log.info('freeswitch_call() CALL_ENDED | CALL {}'.format(self.call.uuid))

                # Handle Call Recording. Send Call Recording Request to API Recording server
                if freeswitch_call_instance.recording_available and settings.record_processing:
                    freeswitch_call_instance.process_recording_file()
                return True
            else:
                notify.call_end(call_uuid=self.call.uuid, result=-1, status='error', hangup_cause='PBX_NOT_CONNECTED')
                raise RuntimeError('freeswitch_call() ESL client not connected {}'.format(self.call.provider.hostname))

        except SoftTimeLimitExceeded, exception:
            if client.connected():
                hangup_cause = 'TASK_LIMIT_EXCEEDED'
                freeswitch_call_instance.force_hangup(self.call.uuid)
            else:
                hangup_cause = 'PBX_NOT_CONNECTED'
            notify.call_end(call_uuid=self.call.uuid, result=-1, status='error', hangup_cause=hangup_cause)
            log.exception('freeswitch_call() {}'.format(exception))

        except ESLError, exception:
            log.exception('freeswitch_call() {}'.format(exception))
            notify.call_end(call_uuid=self.call.uuid, result=-1, status='error', hangup_cause='PBX_NOT_CONNECTED')

        except Exception, exception:
            log.exception('freeswitch_call() {}'.format(exception))

        finally:
            if client.connected():
                client.disconnect()

    def twilio_call(self):
        """
            We dial twilio number
        :return:
        """
        try:
            twilio_call_instance = TwilioCall.TwilioCall()
            twilio_call_instance.call = self.call
            twilio_call_instance.dial()
            log.info('twilio_call() PROCESS_END | CALL {}'.format(self.call.uuid))
            return True
        except Exception, exception:
            log.exception(exception)

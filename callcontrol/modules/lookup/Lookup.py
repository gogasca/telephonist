import pandas as pd

from utils import FileReader


class Lookup(object):
    def __init__(self, country_dialplan):
        self._country_dialplan = country_dialplan
        self._df = None
        self._area_codes = None

    @property
    def area_codes(self):
        return self._area_codes

    @area_codes.setter
    def area_codes(self,area_codes):
        self._area_codes = area_codes

    def load_dialplan(self):
        """

        :param filename:
        :return:
        """

        if self._country_dialplan:
            if FileReader.file_exists(self._country_dialplan):
                self._df = pd.read_csv(self._country_dialplan)
                self.area_codes = self._df['AREA_CODE']
                return self._df
        else:
            print 'process_lookup() No dialplan loaded'

    def check_number(self, destination, max_len=10):
        """

        :param destination:
        :param max_len:
        :return:
        """
        if not isinstance(destination, str):
            return False

        if not len(destination) == max_len:
            return False

        return True

    def extract_area_code(self, destination):
        """

        :param destination:
        :return:
        """

        area_code = destination[:2]
        if area_code in ["33", "55", "81"]:
            pass
        else:
            area_code = destination[:3]

        return area_code

    def extract_office_code(self, destination):
        """

        :param destination:
        :return:
        """
        area_code = self.extract_area_code(destination)
        office_code = None

        if len(area_code) == 2:
            office_code = destination[2:6]
        if len(area_code) == 3:
            office_code = destination[3:6]

        return office_code

    def extract_subscriber(self, destination):
        """

        :param destination:
        :return:
        """

        return destination[6:10]

    def extract_number_tokens(self, destination):
        """

        :param destination:
        :return:
        """
        if self.check_number(destination):
            area_code = self.extract_area_code(destination)
            office_code = self.extract_office_code(destination)
            subscriber_number = self.extract_subscriber(destination)
            return int(area_code), int(office_code), int(subscriber_number)
        else:
            return None, None, None

    def get_subscriber_type(self, destination, area_codes):
        """

        :param destination:
        :return:
        """

        area_code, office_code, subscriber_number = self.extract_number_tokens(destination)
        if not (area_code and office_code and subscriber_number):
            return

        df = self._df

        try:
            if area_code in set(self.area_codes.astype(int)):
                area_code_numbers = df[area_codes == area_code]
                if any(area_code_numbers['OFFICE_CODE'].astype(int) == office_code):
                    matching_records = area_code_numbers[area_code_numbers['OFFICE_CODE'].astype(int) == office_code]
                    # We compare the subscriber number with the start range, then make sure is less than end range.
                    start = subscriber_number >= matching_records['SUBSCRIBER_START']
                    end = subscriber_number <= matching_records['SUBSCRIBER_END']
                    # Perform intersection
                    record_label = matching_records[start & end]['LABEL'].to_string(index=False)
                    # We should return only 1 value
                    if len(record_label) > 0 and record_label != 'Series([], )':
                        return record_label
                    else:
                        return 'INVALID_SUBSCRIBER'
                else:
                    return 'INVALID_OFFICE_CODE'
            else:
                return 'INVALID_AREA_CODE'
        except KeyError, e:
            print e
        except Exception, e:
            print e

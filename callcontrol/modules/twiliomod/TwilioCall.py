import logging
import time
from conf import logger
from conf import settings
from callcontrol import notify
log = logger.LoggerManager().getLogger("__app__")
log.setLevel(level=logging.DEBUG)


class TwilioCall(object):
    """
            We dial twilio number
        :return:
        """

    def __init__(self):
        self._call = None

    @property
    def call(self):
        return self._call

    @call.setter
    def call(self, call):
        self._call = call

    def dial(self):

        if self.call and self.call.call_info:
            log.info('twilio_call() {} {}'.format(self.call.uuid, self.call.call_info))
            call_result = self.call.dial()
            if call_result:
                notify.call_init(call_uuid=self.call.uuid, callsid=call_result.sid)
                duration_check = 1
                if not self.call.duration:
                    log.warn('twilio_call() Using system default duration')
                    self.call.duration = settings.call_duration
                log.info('twilio_call() Processing call: {}|{}| duration: {} seconds'.format(self.call.uuid,
                                                                                             call_result.sid,
                                                                                             self.call.duration))
                while True:
                    if duration_check <= self.call.duration:
                        status = self.call.status()
                        if status:
                            log.info('twilio_call() {}|{}|{}'.format(self.call.uuid, call_result.sid, status))
                        else:
                            log.error('twilio_call() Not able to get status information')
                        if status == 'completed':
                            log.info('twilio_call() Call completed')
                            break
                        if status == 'failed':
                            log.info('twilio_call() Call failed')
                            break
                        if status == 'no-answer':
                            log.info('twilio_call() Call no-answer')
                            break
                        # We introduce a pause before we check the status again.
                        time.sleep(settings.call_status_check)
                        duration_check += 1
                    else:
                        log.warn('twilio_call() Call duration reached {}'.format(duration_check))
                        self.call.hangup()
                        break

                if status == 'completed':
                    notify.call_end(callsid=call_result.sid, result=1)
                elif status == 'no-answer':
                    notify.call_end(callsid=call_result.sid, result=-1)
                elif status == 'failed':
                    notify.call_end(callsid=call_result.sid, result=-1)
                elif status == 'canceled':
                    notify.call_end(callsid=call_result.sid, result=-1)
                else:
                    log.warn('twilio_call() {}'.format(status))
                    notify.call_end(callsid=call_result.sid, result=-1)
                return True
            else:
                notify.call_result(self.call.uuid, -1)
                log.error('twilio_call() Not able to dial call')
                return False
        else:
            log.error('twilio_call() Call object not defined')
            return False



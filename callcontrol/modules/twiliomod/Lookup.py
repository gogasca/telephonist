import ConfigParser

from twilio import TwilioRestException
from twilio.rest.lookups import TwilioLookupsClient

from conf import settings
from utils import FileReader


class Lookup(object):
    def __init__(self):
        self._telephone = None
        self._telephone_info = None
        self._include_carrier_info = False

    @property
    def telephone(self):
        """

        :return:
        """
        return self._telephone

    @telephone.setter
    def telephone(self, telephone):
        """

        :param telephone:
        :return:
        """
        self._telephone = telephone

    @property
    def telephone_info(self):
        """

        :return:
        """
        return self._telephone_info

    @telephone_info.setter
    def telephone_info(self, telephone_info):
        """

        :param telephone_info:
        :return:
        """
        self._telephone_info = telephone_info

    @property
    def include_carrier_info(self):
        """

        :return:
        """
        return self._include_carrier_info

    @include_carrier_info.setter
    def include_carrier_info(self, include_carrier_info):
        """

        :param include_carrier_info:
        :return:
        """
        self._include_carrier_info = include_carrier_info

    def get_info(self, filename=settings.twilio_file):
        """

        :param filename:
        :return:
        """
        if FileReader.file_exists(filename):
            config = ConfigParser.ConfigParser()
            config.read(filename)
            account_sid = config.get('account', 'account')
            auth_token = config.get('token', 'token')

        try:
            client = TwilioLookupsClient(account_sid, auth_token)

            self.telephone_info = client.phone_numbers.get(self.telephone,
                                                           include_carrier_info=self.include_carrier_info)
            if self.telephone_info:
                if self.include_carrier_info:
                    print(self.telephone_info.carrier['type'])
                    print (self.telephone_info.carrier['name'])
                print self.telephone_info.country_code

            return self.telephone_info

        except TwilioRestException, exception:
            print exception

import json
import logging
import math
import time
from functools import partial

from tornado import gen, httpclient
from tornado.ioloop import IOLoop
from tornado.log import gen_log
from tornado.web import RequestHandler, asynchronous

from callcontrol import notify
from conf import logger
from conf import settings
from utils import helper

log = logger.LoggerManager().getLogger("__app__")
log.setLevel(level=logging.DEBUG)


class RecordingRequest(RequestHandler):
    """
        Class which handles Recording process.
        We create a RecordingRequest for each call which was answered.
        Recording file is stored in FreeSwitch/Twilio server
        Recording file is .wav format. For file format and specifications please check README.md
    """

    def __init__(self, uuid, recording_file, recording_url, country_code='1'):
        """

        :param uuid:
        :param recording_file:
        :return:
        """
        self.delay = 0
        self.uuid = uuid
        self.recording_file = None
        self.language = 'en-US'

        self.body = dict()
        if recording_url:
            self.body["RecordingUrl"] = recording_url
        self.body["Uuid"] = self.uuid
        self.body["RecordingFile"] = recording_file
        self.body["CountryCode"] = country_code

    @asynchronous
    def post(self):
        """

        :rtype: object
        """
        IOLoop.current().run_sync(self._process)
        log.info('post() {}  Sending recording request '.format(self.uuid))

    @gen.coroutine
    def _process(self):
        """

        :return:
        """
        try:
            http_client = httpclient.AsyncHTTPClient()
            log.info("RecordingRequest() Record request: {} {}".format(self.uuid, self.body))
            headers = {'Content-Type': 'application/json'}
            request = httpclient.HTTPRequest(auth_username=settings.api_account,
                                             auth_password=settings.api_password,
                                             url=settings.api_record_request,
                                             body=json.dumps(self.body),
                                             headers=headers,
                                             request_timeout=15,
                                             method="POST")
            yield http_client.fetch(request, callback=partial(self.handle_response, self.uuid))

        except httpclient.HTTPError, exception:
            log.exception("RecordingRequest() ".format(exception))

        except Exception, exception:
            log.exception("RecordingRequest() ".format(exception))

    @staticmethod
    def handle_response(uuid, response):
        """

        :param uuid:
        :param response:
        :return:
        """

        gen_log.info("handle_response() UUID: {} Recording API Response: {}".format(uuid, response.code))
        if response.code != 202:
            result = uuid + "," + str(response.code) + "\n"
            helper.failed_requests(result, 2)


class FreeSwitchCall(object):
    """
        originate { ignore_early_media=true,
                    origination_uuid=ABCDE,
                    originate_timeout=18,
                    media_bug_answer_req=True,
                    origination_caller_id_number=+18623079305,
                    RECORD_STEREO=true,
                    execute_on_answer='record_session /usr/local/freeswitch/recordings/ABCDE.wav'}
                    sofia/gateway/asterisk/+14082186575 &park()
    """

    def __init__(self, connection=None):
        self._call = None
        self._connection = connection
        self._connected = False
        self._media_detected = False
        self._call_duration = settings.sip_call_duration
        self._recording_enable = True
        self._recording_server = None
        self._recordings_dir = settings.record_directory
        self._record_file_name = None
        self._recording_url = None
        self._recording_available = False

    @property
    def connected(self):
        return self._connected

    @connected.setter
    def connected(self, connected):
        self._connected = connected

    @property
    def media_detected(self):
        return self._media_detected

    @media_detected.setter
    def media_detected(self, media_detected):
        self._media_detected = media_detected

    @property
    def connection(self):
        return self._connection

    @property
    def call(self):
        return self._call

    @call.setter
    def call(self, call):
        self._call = call

    @property
    def recording_available(self):
        return self._recording_available

    @recording_available.setter
    def recording_available(self, recording_available):
        """

        :param recording_available:
        :return:
        """
        self._recording_available = recording_available

    @property
    def recording_server(self):
        """

        :return:
        """
        return self._recording_server

    @recording_server.setter
    def recording_server(self, recording_server):
        """

        :param server:
        :return:
        """
        self._recording_server = recording_server

    @property
    def record_file_name(self):
        """

        :return:
        """
        return self._record_file_name

    @record_file_name.setter
    def record_file_name(self, record_file_name):
        """

        :param record_file_name:
        :return:
        """
        self._record_file_name = record_file_name

    @property
    def recording_url(self):
        return self._recording_url

    @recording_url.setter
    def recording_url(self, recording_url):
        self._recording_url = recording_url

    def process_recording_file(self):
        """

        :return:
        """
        recording = RecordingRequest(self.call.uuid, self.record_file_name, self.recording_url,
                                     self.call.country_code[1:])
        recording.post()

    def force_hangup(self, uuid):
        """

        :param uuid:
        :return:
        """
        if self.connection.connected() and uuid:
            log.info('force_hangup() HANGUP | CALL {}'.format(uuid))
            self.connection._con.api("uuid_kill", uuid)
            return True
        else:
            raise RuntimeError('force_hangup() Unable to terminate call')

    def dial(self,
             params="",
             caller_id=settings.sip_callerid,
             record=False,
             app=None):
        """

        :param params:
        :param caller_id:
        :param record:
        :param app:
        :return:
        """

        call_start = 0
        call_end = 0
        hangup_cause = None
        status = 'initiated'
        session_progress = False
        tone_detected = None
        session_progress_message = 0
        busy_tone = 0
        fax_tone = 0
        reorder_tone = 0
        ring_back_tone = 0
        sit_tone = 0

        if self.call and self.call.call_info:
            log.info('dial() {} {}'.format(self.call.uuid, self.call.call_info))
        events = "BACKGROUND_JOB " \
                 "DETECTED_TONE " \
                 "CHANNEL_OUTGOING " \
                 "CHANNEL_CREATE " \
                 "CHANNEL_ANSWER " \
                 "CHANNEL_PROGRESS_MEDIA " \
                 "RECORD_START " \
                 "CHANNEL_HANGUP " \
                 "CHAN_NOT_IMPLEMENTED "

        try:

            self.connection.events("plain", events)
            # Update callerid
            if self.call.call_info["from"]:
                caller_id = str(self.call.call_info["from"])

            # print 'dial() Created UUID: {}'.format(uuid)
            sip_header_params = "sip_h_X-UUID=" + self.call.uuid + \
                                ",sip_h_X-Campaign=" + str(self.call.call_info["campaign"])

            cmd = "originate {" + sip_header_params + "," + params + \
                  ",origination_uuid=" + self.call.uuid + \
                  ",origination_caller_id_number=" + caller_id + \
                  ",country_code_tone=" + self.call.country_code[1:]

            if record:
                log.info("dial() ENABLE_RECORDING | CALL {} ".format(self.call.uuid))
                self.record_file_name = str(self.call.call_info["campaign"]) + "_" + str(
                    self.call.call_info["to"]) + "_" + self.call.uuid + ".wav"
                cmd += ",RECORD_STEREO=true,execute_on_answer_2=\'record_session " + self._recordings_dir + \
                       self.record_file_name + "\'}"
                self._recording_enable = True
            else:
                cmd += "}"

            # Dial out
            cmd += "sofia/gateway/" + self.call.provider.gateway + "/" + \
                   str(self.call.call_info["to"]) + app.name

            log.info(cmd)
            res = self.connection.bgapi(cmd)
            job_uuid = res.getHeader("Job-UUID")
            log.info('dial() UUID | CALL {} Job-UUID: {} to: {}'.format(self.call.uuid,
                                                                        job_uuid,
                                                                        str(self.call.call_info["to"])))

            time.sleep(0.05)
            init_time = time.time()
            # Update database to update call with INITIALIZED status
            notify.call_init(uuid=self.call.uuid)
            while time.time() - init_time < settings.max_call_duration:
                reply = self.connection.recvEventTimed(1000)
                if reply:
                    ev_name = reply.getHeader('Event-Name')
                    # log.info(ev_name)
                    # log.info(reply.serialize("xml"))

                    if ev_name == 'BACKGROUND_JOB':
                        if reply.getHeader("Channel-Call-UUID") == self.call.uuid:
                            if reply.getHeader("Job-UUID") == job_uuid:
                                call_result = reply.getBody()
                                log.info("dial() {} CALL RESULT {} TO: {}".format(ev_name,
                                                                                  call_result,
                                                                                  str(self.call.call_info["to"])))
                                if "-ERR NO_ANSWER" in call_result:
                                    hangup_cause = "NO_ANSWER"
                                    break
                                elif "-ERR" in call_result:
                                    break

                    elif ev_name == 'CHANNEL_OUTGOING':
                        if reply.getHeader("Channel-Call-UUID") == self.call.uuid:
                            status = 'queued'

                    elif ev_name == 'CHANNEL_CREATE':
                        if reply.getHeader("Channel-Call-UUID") == self.call.uuid:
                            status = 'no-answer'

                    elif ev_name == 'CHANNEL_ANSWER':
                        if reply.getHeader("Channel-Call-UUID") == self.call.uuid:
                            log.info('dial() {} | CALL {} to: {} has been answered'.format(ev_name,
                                                                                           self.call.uuid,
                                                                                           str(self.call.call_info[
                                                                                                   "to"])))
                            call_start = reply.getHeader('Event-Date-Timestamp')
                            if reply.getHeader("Answer-State") == 'answered':
                                log.info('dial() {} | CALL {} Channel is active'.format(ev_name, self.call.uuid))
                                self.connected = True
                                status = 'in-progress'
                                notify.call_media_detected(self.call.uuid, status)

                    elif ev_name == 'CHANNEL_PROGRESS_MEDIA':
                        if reply.getHeader("Channel-Call-UUID") == self.call.uuid:
                            session_progress = True
                            if session_progress_message == 0:
                                log.info('dial() {} | CALL {}'.format(ev_name, self.call.uuid))
                            session_progress_message += 1

                    elif ev_name == 'DETECTED_TONE':
                        if reply.getHeader("Channel-Call-UUID") == self.call.uuid:

                            tone_detected = reply.getHeader('Detected-Tone')
                            if tone_detected == "CED_TONE":
                                # Fax tone is detected in Freeswitch tag call accordingly.
                                if fax_tone == 0:
                                    log.warn(
                                        'dial() {} | CALL {} FAX {}'.format(ev_name, self.call.uuid, tone_detected))
                                fax_tone += 1
                                notify.call_fax(self.call.uuid)

                            if tone_detected == "RING_TONE":
                                if ring_back_tone == 0:
                                    log.info('dial() {} | CALL {} {}'.format(ev_name, self.call.uuid, tone_detected))
                                ring_back_tone += 1

                            if tone_detected == "BUSY_TONE":
                                if busy_tone == 0:
                                    log.warn('dial() {} | CALL {} {}'.format(ev_name, self.call.uuid, tone_detected))
                                busy_tone = +1

                            if tone_detected == "REORDER_TONE":
                                if reorder_tone == 0:
                                    log.warn('dial() {} | CALL {} {}'.format(ev_name, self.call.uuid, tone_detected))
                                reorder_tone += 1

                            if tone_detected == "SIT":
                                if sit_tone == 0:
                                    log.warn('dial() {} | CALL {} {}'.format(ev_name, self.call.uuid, tone_detected))
                                sit_tone += 1

                    elif ev_name == 'RECORD_START':
                        if self.call.uuid == reply.getHeader("Channel-Call-UUID"):
                            log.info(
                                'dial() {} | CALL {} File: {} Recording started'.format(ev_name, self.call.uuid,
                                                                                        self.record_file_name))
                            self.recording_server = reply.getHeader("FreeSWITCH-IPv4")
                            self.recording_available = True
                            if self.recording_server:
                                self.recording_url = settings.record_scheme + "://" + \
                                                     self.recording_server + ":" + settings.record_server_port + "/" + \
                                                     self.record_file_name

                            notify.call_recording_details(uuid=self.call.uuid,
                                                          recording_url=self.recording_url,
                                                          recording_file=self.record_file_name)

                    elif ev_name == 'CHANNEL_HANGUP':  # https://wiki.freeswitch.org/wiki/Hangup_Causes
                        if self.call.uuid == reply.getHeader("Channel-Call-UUID"):
                            # Default state as call completed
                            status = 'completed'

                            # Get Variables
                            hangup_cause = reply.getHeader('Hangup-Cause')
                            call_end = reply.getHeader('Event-Date-Timestamp')
                            variable_twilio_callsid = reply.getHeader('variable_sip_ph_X-Twilio-CallSid')
                            if variable_twilio_callsid is None:
                                variable_twilio_callsid = reply.getHeader('variable_sip_rh_X-Twilio-CallSid')
                            variable_sip_term_status = reply.getHeader('variable_sip_term_status')
                            variable_sip_invite_failure_phrase = reply.getHeader('variable_sip_invite_failure_phrase')
                            variable_sip_call_id = reply.getHeader('variable_sip_call_id')

                            # CALL NO ANSWER
                            if hangup_cause == 'NO_ANSWER':
                                status = 'no-answer'

                            # CALL NO ANSWER + RINGTONE DETECTED + session progress
                            if hangup_cause == 'NORMAL_CLEARING' and call_start == 0 and session_progress and \
                                            ring_back_tone >= 2:
                                log.warn('dial() {} | CALL {} Cause overwrite: {} to NO_ANSWER'.format(ev_name,
                                                                                                       self.call.uuid,
                                                                                                       hangup_cause))
                                status = 'no-answer'
                                hangup_cause = 'NO_ANSWER'

                            # CALL NO ANSWER + RINGTONE DETECTED + No session progress
                            if hangup_cause == 'NORMAL_CLEARING' and call_start == 0 and ring_back_tone >= 2:
                                log.warn('dial() {} | CALL {} Cause overwrite: {} to NO_ANSWER'.format(ev_name,
                                                                                                       self.call.uuid,
                                                                                                       hangup_cause))
                                status = 'no-answer'
                                hangup_cause = 'NO_ANSWER'

                            # CALL NO ANSWER + REORDER TONE DETECTED + Session progress
                            if hangup_cause == 'NORMAL_CLEARING' and call_start == 0 and session_progress and \
                                            ring_back_tone == 0 and tone_detected in ["BUSY_TONE", "REORDER_TONE",
                                                                                      "SIT"]:

                                status = 'failed'
                                # US and Canada
                                if self.call.country_code[1:] == '1' and tone_detected == "BUSY_TONE":
                                    hangup_cause = 'USER_BUSY'
                                    status = 'busy'
                                else:
                                    hangup_cause = 'INVALID_DESTINATION'
                                log.warn(
                                    'dial() {} | CALL {} Progress Tone {}. Change: {} to {}.'.format(
                                        ev_name,
                                        self.call.uuid,
                                        tone_detected,
                                        hangup_cause))

                            # CALL NO ANSWER + NO TONE DETECTED + Session progress (In band announcement)
                            if hangup_cause == 'NORMAL_CLEARING' and call_start == 0 and session_progress and \
                                            tone_detected is None:
                                log.warn(
                                    'dial() {} | CALL {} Cause overwrite: {} to INVALID_DESTINATION'.format(ev_name,
                                                                                                            self.call.uuid,
                                                                                                            hangup_cause))
                                status = 'failed'
                                hangup_cause = 'INVALID_DESTINATION'

                            # CALL IS CONNECTED. TONE DETECTED
                            if tone_detected in ["BUSY_TONE", "REORDER_TONE", "SIT"] and self.connected and \
                                            call_end > 0 and call_start > 0:
                                log.error('dial() CALL_FAILED | CALL {} TONE {}'.format(self.call.uuid, tone_detected))
                                status = 'failed'

                            # MEDIA TIMEOUT
                            if hangup_cause == 'MEDIA_TIMEOUT':
                                log.warn('dial() MEDIA_TIMEOUT | CALL {} failed'.format(self.call.uuid))
                                status = 'error'

                            # NORMAL DISCONNECT
                            if hangup_cause == 'ALLOTTED_TIMEOUT':
                                log.warn('dial() DURATION_EXPIRED | CALL {} duration expired.'.format(self.call.uuid))

                            # SIP ERRORS
                            if variable_sip_term_status:
                                if int(variable_sip_term_status) >= 400:
                                    log.error('dial() SIP_ERROR | CALL {} CODE {}'.format(self.call.uuid,
                                                                                          variable_sip_term_status))
                                    status = 'error'
                                    # CALL RINGS THEN BUSY TONE
                                    if int(variable_sip_term_status) == 486 and ring_back_tone >= 3:
                                        status = 'no-answer'
                                        hangup_cause = 'NO_ANSWER'
                                    else:
                                        # UPDATE SIP MESSAGE CODE AND ERROR
                                        if variable_sip_invite_failure_phrase:
                                            sip_error = str(variable_sip_term_status) + " " + \
                                                        variable_sip_invite_failure_phrase
                                        notify.call_sip_error(self.call.uuid, sip_error)

                            log.info('dial() {} | CALL {} Finalized. CALLSID: {} Cause: {}'.format(ev_name,
                                                                                                   self.call.uuid,
                                                                                                   variable_twilio_callsid,
                                                                                                   hangup_cause))
                            # Update TWILIO CallSID
                            notify.call_update_callid(self.call.uuid, variable_twilio_callsid, variable_sip_call_id)
                            break

                    elif ev_name == 'CHAN_NOT_IMPLEMENTED':
                        if reply.getHeader("Channel-Call-UUID") == self.call.uuid:
                            log.error(ev_name)
                            break

            return True

        except KeyboardInterrupt, exception:
            log.exception('KeyboardInterrupt {}'.format(exception))

        except Exception, exception:
            log.exception('Exception {}'.format(exception))

        finally:

            log.info('dial() FINALIZED | CALL {} STATUS {}.'.format(self.call.uuid, status))
            if self.call.uuid:
                summary = 'Fax: {} RB: {} BZ: {} R: {} SIT: {} 183_SDP: {}'.format(fax_tone,
                                                                                   ring_back_tone,
                                                                                   busy_tone,
                                                                                   reorder_tone,
                                                                                   sit_tone,
                                                                                   session_progress_message)

                log.info('dial() CALL SUMMARY | CALL {} {}'.format(self.call.uuid, summary))
                # NO HANGUP_CAUSE
                if hangup_cause is None:
                    if status == 'no-answer' and busy_tone == 0 and reorder_tone == 0 and sit_tone == 0:
                        if session_progress and ring_back_tone >= 1:
                            hangup_cause = 'NO_ANSWER'
                        if not session_progress:
                            hangup_cause = 'NO_ANSWER'
                    else:
                        hangup_cause = 'UNKNOWN'
                    log.error('dial() NO HANGUP_CAUSE EVENT | CALL {}'.format(self.call.uuid))

                # CALL COMPLETED
                if status == 'completed':
                    if call_start == 0:
                        log.warn('dial() {} Unable to get call start time'.format(self.call.uuid))
                        duration = settings.sip_call_duration
                    else:
                        if call_end > 0 and call_start > 0:
                            duration = int(math.floor((float(call_end) - float(call_start)) / 1000000))

                    notify.call_end(call_uuid=self.call.uuid,
                                    result=1,
                                    duration=duration,
                                    status=status,
                                    hangup_cause=hangup_cause,
                                    summary=summary)
                # CALL FAILED
                elif status == 'failed':
                    if call_end > 0 and call_start > 0:
                        duration = int(math.floor((float(call_end) - float(call_start)) / 1000000))
                    else:
                        duration = 0

                    notify.call_end(call_uuid=self.call.uuid,
                                    result=-1,
                                    duration=duration,
                                    status=status,
                                    hangup_cause=hangup_cause,
                                    summary=summary)

                # CALL IN PROGRESS
                elif status == 'in-progress':
                    # DIDN'T GET HANGUP CAUSE (Example: 481 Call leg/Transaction does not exist)
                    status = 'error'
                    if call_end > 0 and call_start > 0:
                        duration = int(math.floor((float(call_end) - float(call_start)) / 1000000))
                    else:
                        duration = 0

                    notify.call_end(call_uuid=self.call.uuid,
                                    result=-1,
                                    duration=duration,
                                    status=status,
                                    hangup_cause=hangup_cause,
                                    summary=summary)
                # CALL ERROR
                elif status == 'error':
                    # SIP ERROR
                    notify.call_end(call_uuid=self.call.uuid,
                                    result=-1,
                                    status=status,
                                    hangup_cause=hangup_cause,
                                    summary=summary)

                # CALL NO ANSWERED
                elif status == 'no-answer':

                    notify.call_end(call_uuid=self.call.uuid,
                                    result=-1,
                                    status=status,
                                    hangup_cause=hangup_cause,
                                    summary=summary)


                else:
                    if status:
                        log.warn('dial() Status {}'.format(status))
                    else:
                        status = 'unknown'

                    notify.call_end(call_uuid=self.call.uuid,
                                    result=-1,
                                    status=status,
                                    hangup_cause=hangup_cause,
                                    summary=summary)
            else:
                notify.call_end(call_uuid=self.call.uuid,
                                result=-1,
                                status=status)

import ConfigParser
import collections
import copy
import csv
import json
import logging
import os
import time
from functools import partial

from celery.task import task
from tornado import gen, httpclient, ioloop
from tornado.log import gen_log
from twilio import TwilioException
from twilio import TwilioRestException
from twilio.rest import TwilioRestClient

from callcontrol import BlackList
from callcontrol import CallProcessor, SpeechRecognition
from callcontrol.modules.lookup import Lookup
from conf import logger
from conf import settings
from error import exceptions
from text_analysis import cosine_similarity, levenshtein_distance
from utils import FileReader
from utils import helper
from utils import yaml_processor
from utils.twilio import send_sms

log = logger.LoggerManager().getLogger("__app__")
log.setLevel(level=logging.DEBUG)


class BacklogClient(object):
    MAX_CONCURRENT_REQUESTS = settings.api_client_max_requests

    def __init__(self, ioloop):
        self.ioloop = ioloop
        self.client = httpclient.AsyncHTTPClient(max_clients=self.MAX_CONCURRENT_REQUESTS)
        self.client.configure(None, defaults=dict(connect_timeout=15, request_timeout=30))
        self.backlog = collections.deque()
        self.concurrent_requests = 0

    def __get_callback(self, function):
        def wrapped(*args, **kwargs):
            self.concurrent_requests -= 1
            self.try_run_request()
            return function(*args, **kwargs)

        return wrapped

    def try_run_request(self):
        while self.backlog and self.concurrent_requests < self.MAX_CONCURRENT_REQUESTS:
            request, callback = self.backlog.popleft()
            self.client.fetch(request, callback=callback)
            self.concurrent_requests += 1

    def fetch(self, request, callback=None):
        wrapped = self.__get_callback(callback)

        self.backlog.append((request, wrapped))
        self.try_run_request()


class TornadoBacklog:
    def __init__(self, campaign_instance):

        self.queue = 0
        self.debug = 1
        self.delay = 1.0
        self.toProcess = list()
        self.api_errors = 0

        log.info('TornadoBacklog() Initialized')
        if campaign_instance:
            # =========================================================
            # Create HTTP Async - Non blocking client
            # =========================================================

            if not FileReader.check_fileformat(campaign_instance.filename):
                print 'start_campaign() Invalid File format'

            num_of_records = FileReader.mapcount(campaign_instance.filename)
            log.info("start_campaign() Provider: {}".format(campaign_instance.provider.name))
            log.info("start_campaign() Campaign: {}".format(campaign_instance.reference))
            log.info("start_campaign() Number of Phones: {} | Filename {}".format(
                num_of_records,
                campaign_instance.filename))
            if campaign_instance.cps is None:
                campaign_instance.cps = settings.cps
            log.info("start_campaign() Calls per second: {}".format(campaign_instance.cps))

            if campaign_instance.country_code is None:
                campaign_instance.country_code = settings.call_country_code
            log.info("start_campaign() Country code: {}".format(campaign_instance.country_code))
            log.info("start_campaign() Test: {}".format(campaign_instance.test))
            log.info("start_campaign() Recording enable: {}".format(campaign_instance.record))
            log.info("start_campaign() Blacklist enable: {}".format(campaign_instance.blacklist))
            log.info("start_campaign() Block duplicates: {}".format(settings.call_block_duplicates))
            log.info("start_campaign() Lookup enable: {}".format(campaign_instance.lookup))
            log.info("start_campaign() Transcription enable: {}".format(campaign_instance.transcription))
            log.info("start_campaign() Configuration: {}".format(campaign_instance.provider.config_file))

            body = dict()
            body["campaign"] = campaign_instance.reference

            if num_of_records > settings.max_calls_per_campaign:
                raise RuntimeError('start_campaign() Records {} > MAX_CALLS_PER_CAMPAIGN {}'.format(num_of_records,
                                                                                                    settings.max_calls_per_campaign))
            if num_of_records <= 0:
                raise RuntimeError('start_campaign() No records found')

            # =========================================================
            # Define call body
            # =========================================================

            if campaign_instance.provider.name == "sip":
                body["provider"] = "sip"
                body["duration"] = settings.sip_call_duration
                body["test"] = campaign_instance.test
                body["country_code"] = campaign_instance.country_code
                body["record"] = campaign_instance.record
                body["transcription"] = campaign_instance.transcription

            if campaign_instance.provider.name == "twilio":
                body["provider"] = "twilio"
                body["from"] = settings.twilio_from
                body["url"] = settings.twilio_url
                body["status_callback"] = settings.twilio_statuscallback
                body["duration"] = settings.twilio_call_duration
                body["test"] = campaign_instance.test

        # =========================================================
        # Process Blacklist codes
        # =========================================================
        if campaign_instance.blacklist:
            log.warn('start_campaign() Blacklist feature is enabled')
            blacklist_instance = BlackList.BlackList()
            blacklist_instance.blacklist_file = settings.filepath + 'conf/providers/blacklists/' + \
                                                get_provider_property(campaign_instance.provider.config_file,
                                                                      'blacklist_file')
            blacklist_instance.process_blacklist()
            log.warn('start_campaign() Blacklist file {} '.format(blacklist_instance.blacklist_file))

        call_records = csv.reader(open(campaign_instance.filename))
        # =========================================================
        # Process Duplicates
        # =========================================================
        if settings.call_block_duplicates:
            entries = set()
        else:
            log.warn('start_campaign() Duplicate numbers allowed. System may dial same number more than once...')
            entries = list()

        for call_record in call_records:
            if settings.call_block_duplicates:
                if len(call_record) == 1:
                    key = (call_record[0])

                if len(call_record) == 2:
                    key = (call_record[0], call_record[1])

                if key not in entries:
                    entries.add(key)
            else:
                if len(call_record) == 1:
                    entries.append(call_record[0])
                if len(call_record) == 2:
                    entries.append(call_record[0])

        call_requests = list()

        # =========================================================
        # Initialize Lookup service
        # =========================================================

        if campaign_instance.lookup:
            if campaign_instance.country_code == "+52":
                campaign_instance.lookup_service = Lookup.Lookup(settings.lookup_mexico)
                campaign_instance.lookup_service.load_dialplan()

        # =========================================================
        # Define call body for API Call. Shallow copy of objects
        # =========================================================

        for entry in entries:
            prefix = ''
            if isinstance(entry, tuple):
                called_number = entry[0]
                description = entry[1]
            else:
                called_number = entry
                description = ''
            # =========================================================
            # Blacklist
            # =========================================================
            if campaign_instance.blacklist:
                # Check if number is blacklisted
                if blacklist_instance.is_blacklisted(called_number, campaign_instance.country):
                    log.warn('start_campaign() Blacklisted number {}'.format(called_number))
                    body['blacklist'] = True

            # =========================================================
            # Service Lookup
            # =========================================================
            if campaign_instance.lookup:
                ret = process_lookup(campaign_instance.country, campaign_instance.lookup_service, called_number)
                if ret > 0:
                    log.info('start_campaign() Lookup service found match {}'.format(called_number))
                    prefix = settings.country_prefix[campaign_instance.country_code]
                if ret < 0:
                    log.error('start_campaign() Invalid destination {}'.format(called_number))
                    continue

            body["description"] = description
            body["to"] = campaign_instance.country_code + prefix + called_number
            body["from"] = helper.get_calling_number(number_list=settings.sip_from,
                                                     country_code=campaign_instance.country_code,
                                                     smart_match=True,
                                                     destination=called_number)
            newbody = copy.copy(body)
            call_requests.append(newbody)

        self.delay = helper.get_cps_delay(cps=campaign_instance.cps)
        log.info('start_campaign() CPS : {}'.format(campaign_instance.cps))
        log.info('start_campaign() CPS delay: {}'.format(self.delay))
        self.toProcess = call_requests
        log.info("start_campaign() Start call processing...")

    @gen.coroutine
    def launch(self):
        """

        :return:
        """

        log.info('launch() Starting call requests...')
        self.ioloop = ioloop.IOLoop.current()
        self.backlog = BacklogClient(self.ioloop)

        while True:
            try:
                yield gen.sleep(self.delay)
                gen_log.info("start_campaign() Calls in Queue: {}".format(len(self.toProcess)))
                call_request = self.toProcess.pop()
                gen_log.info("start_campaign() Call request: {}".format(call_request))
                headers = {'Content-Type': 'application/json'}
                request = httpclient.HTTPRequest(auth_username=settings.api_account,
                                                 auth_password=settings.api_password,
                                                 url=settings.api_call_request,
                                                 body=json.dumps(call_request),
                                                 headers=headers,
                                                 connect_timeout=15,
                                                 request_timeout=30,
                                                 method="POST")

                self.backlog.fetch(request, callback=partial(self.handle_request, call_request["to"]))

            except httpclient.HTTPError, exception:
                gen_log.exception("start_campaign() ".format(exception))
                self.api_errors += 1
                if self.handle_api_errors(self.api_errors):
                    break
            except IndexError:
                gen_log.info(
                    'start_campaign() Campaign call requests completed. API Errors: {}'.format(self.api_errors))

                break

        gen_log.info("start_campaign() Waiting for active call requests to complete...")
        yield gen.sleep(settings.loop_delay)

    def handle_api_errors(self, api_errors):
        """

        :param api_errors:
        :return:
        """
        if api_errors >= settings.api_error_limit:
            gen_log.error("handle_api_errors() API Exceed Error limit: {} Hostname: {}".format(settings.api_error_limit,
                                                                                               os.uname()[1]))
            send_sms.send_sms_alert("API Exceed Error limit: {} Hostname: {}".format(settings.api_error_limit,
                                                                                     os.uname()[1]))
            return True

    def handle_request(self, number, response):
        """

        :param number:
        :param response:
        :return:
        """

        gen_log.info("handle_request() Number: {} API Response: {} {}".format(number,
                                                                              response.code,
                                                                              response.body))
        if response.code != 202:
            gen_log.error("handle_request() Error")
            result = number + "," + str(response.code) + "," + str(response.body) + "\n"
            helper.failed_requests(result, 1)


def process_lookup(country, lookup_service, destination):
    """

    :param lookup_service:
    :param destination:
    :return:
    """
    if lookup_service and destination:
        if country == "+52":
            phone_type = lookup_service.get_subscriber_type(destination, lookup_service.area_codes)
            if phone_type == 'MOVIL':
                log.info('process_lookup() Number: {} Type: {}'.format(destination, phone_type))
                return 1
            if phone_type in ['INVALID_SUBSCRIBER', 'INVALID_OFFICE_CODE', 'INVALID_AREA_CODE']:
                log.error('process_lookup() Number: {} Type: {}'.format(destination, phone_type))
                helper.failed_requests(destination, 5)
                return -1
    return 0


@task(name='process_call', queue='gold', bind=True, default_retry_delay=30, max_retries=3,
      soft_time_limit=settings.max_call_duration)
def process_call(self, callinstance):
    """

    :param self:
    :param callinstance:
    :return:
    """

    if get_provider(callinstance.provider):
        # print 'Provider: {}'.format(callinstance.provider.name)
        pass
    else:
        raise exceptions.InvalidProvider('process_call() Invalid provider instance')

    callprocessor = CallProcessor.CallProcessor()

    if callinstance.provider:

        # =========================================================
        # Sip call
        # =========================================================

        if callinstance.provider.name == "sip":
            callprocessor.call = callinstance
            result = None
            if callinstance.blacklist:
                if callprocessor.blacklist_call():
                    log.warn("process_call() | CALL {} Blacklisted".format(callinstance.uuid))
                    result = 'completed'
                else:
                    log.error("process_call() | CALL {} Error detected".format(callinstance.uuid))
                    result = 'failed'
                if self.request.id:
                    return {'result': result}

            if settings.sip_provider == "freeswitch":
                if callprocessor.freeswitch_call():
                    log.info("process_call() | CALL {} Completed".format(callinstance.uuid))
                    result = 'completed'
                else:
                    log.error("process_call() | CALL {} Error detected".format(callinstance.uuid))
                    result = 'failed'
            if self.request.id:
                return {'result': result}

        # =========================================================
        # Twilio call
        # =========================================================

        if callinstance.provider.name == "twilio":
            try:
                callinstance.client = TwilioRestClient(account=callinstance.provider.account_name,
                                                       token=callinstance.provider.token)
                callprocessor.call = callinstance
                result = None
                if callprocessor.twilio_call():
                    log.info("process_call() | CALL {} Completed".format(callinstance.uuid))
                    result = 'completed'
                else:
                    log.error("process_call() | CALL {} Error detected".format(callinstance.uuid))
                    result = 'failed'
            except TwilioRestException, exception:
                log.exception("TwilioRestException {}".format(exception))
            except TwilioException, exception:
                log.exception("TwilioException {}".format(exception))
            if self.request.id:
                return {'result': result}

        raise exceptions.InvalidProvider('process_call() Provider not found: {}'.format(callinstance.provider.name))

    else:
        raise exceptions.InvalidProvider('process_call() Invalid provider instance')





@task(name='process_recording', queue='silver', bind=True, default_retry_delay=30, max_retries=3)
def process_recording(self, callrecord_instance):
    """

    :param self:
    :param callrecord_instance:
    :return:
    """
    if callrecord_instance.recording_url:
        # Update recording url, recording duration
        if callrecord_instance.recording_type != "sip":
            callrecord_instance.update_database_record()  # Twilio, update record in database

        # callrecord_instance.download_record_file()
        # if callrecord_instance.is_download_completed:
        #     log.info('process_recording() File download completed')
        #     result = "completed"
        # else:
        #     result = "failed"

        if settings.speech_recognition:
            log.info('process_recording() Speech Recognition')
            sr_instance = SpeechRecognition.SpeechRecognition()
            if callrecord_instance.recording_type != "sip":
                sr_instance.callsid = callrecord_instance.callsid
            else:
                sr_instance.uuid = callrecord_instance.uuid

            sr_instance.audiofile = callrecord_instance.recording_file
            sr_instance.country_code = callrecord_instance.country_code

            #text = sr_instance.audio_to_text()
            text = sr_instance.transcribe_audio_google()

            if text:
                # Update text translated into database
                sr_instance.update_database_record(code=1)

                ###################################################################################################
                # This section compares the text obtained from Media/Voice translated by Google/Local Speech engine
                # to a fixed description. Example: Holiday Inn vs Holiday In (Google translation)
                ###################################################################################################

                if settings.text_analysis:
                    # Get company information if any
                    if callrecord_instance.recording_type != "sip":  # Twilio
                        callrecord_instance.description = helper.query_database(
                            """SELECT description FROM calls WHERE callsid='""" + callrecord_instance.callsid + "'")
                    else:
                        callrecord_instance.description = helper.query_database(
                            """SELECT description FROM calls WHERE uuid='""" + callrecord_instance.uuid + "'")

                    if callrecord_instance.description:
                        log.info('process_recording() Text Analysis')
                        analysis = dict()
                        analysis["cosine"] = cosine_similarity.cosine_sim(text, callrecord_instance.description)
                        analysis["fuzzy_match"] = levenshtein_distance.fuzzy_match(text,
                                                                                   callrecord_instance.description)
                        analysis["levenshtein"] = levenshtein_distance.levenshtein(text,
                                                                                   callrecord_instance.description)
                        sr_instance.analysis = analysis
                        sr_instance.update_database_record(code=2)

                    else:
                        log.info('No description found')
                else:
                    log.warn('Text analysis is disabled')
            else:
                log.warn('No text found')

        if self.request.id:
            return {'result': 'completed'}  # TODO Evaluate result of translation and pass proper code

    else:
        raise exceptions.RecordingNotFound('Recording not found')


@task(name='process_campaign', queue='bronze', bind=True, default_retry_delay=30, max_retries=3)
def process_campaign(self, campaign_instance):
    """

    :param self:
    :param campaign_instance:
    :return:
    """

    start_time = time.time()
    gen_log.info('process_campaign() Initializing...')
    campaign = TornadoBacklog(campaign_instance)
    send_sms.send_sms_alert(body='Campaign: {} started'.format(campaign_instance.reference))
    ioloop.IOLoop.current().run_sync(lambda: campaign.launch())
    elapsed_time = time.time() - start_time
    gen_log.info('Process took %f seconds processed ' % (elapsed_time))
    campaign_instance.terminate()
    send_sms.send_sms_alert(body='Campaign: {} completed'.format(campaign_instance.reference))


def get_provider_property(provider_file, property):
    """

    :param provider:
    :param property:
    :return:
    """
    return yaml_processor.YamlProcessor(provider_file).get_property('provider', property)


def get_provider(provider):
    """

    :param name:
    :return:
    """
    try:

        if provider.name:
            # Get PBX information from YAML file
            if provider.test and settings.provider_test:
                log.warn(
                    'get_provider() settings.provider_test on. Please disable it to validate production infrastructure')
                filename = settings.filepath + settings.provider_folder + provider.name + ".test.yaml"
            else:
                filename = settings.filepath + settings.provider_folder + provider.name + ".yaml"

            if FileReader.file_exists(filename):
                if provider.name == "twilio":
                    config = ConfigParser.ConfigParser()
                    config.read(filename)
                    provider.account_name = config.get(provider.name, 'account')
                    provider.token = config.get(provider.name, 'token')

                if provider.name == "sip":
                    if settings.sip_provider == "freeswitch":
                        provider.host = get_provider_property(filename, 'ip')
                        provider.port = get_provider_property(filename, 'port')
                        provider.password = get_provider_property(filename, 'password')

                        if provider.blacklist:
                            provider.blacklist_file = get_provider_property(filename, 'blacklist_file')
                        if provider.test:
                            provider.gateway = get_provider_property(filename, 'test_gateway')
                        else:
                            provider.gateway = get_provider_property(filename, 'gateway')

                return True
            else:
                raise exceptions.InvalidConfigurationFile(filename + ' not found')
        else:
            log.error("get_provider() No Provider name defined")

    except ConfigParser.NoOptionError, e:
        log.exception(e)
    except ConfigParser.NoSectionError, e:
        log.exception(e)

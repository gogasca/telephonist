import csv
from callcontrol.modules.lookup import Lookup
from conf import settings


class BlackList():
    def __init__(self):
        self._blacklist_file = None
        self._blacklist_codes = list()
        self._enabled = True

    @property
    def enabled(self):
        return self._enabled

    @enabled.setter
    def enabled(self, enabled):
        self._enabled = enabled

    @property
    def blacklist_file(self):
        return self._blacklist_file

    @blacklist_file.setter
    def blacklist_file(self, blacklist_file):
        self.blacklist_file = blacklist_file

    @property
    def blacklist_codes(self):
        return self._blacklist_codes

    @blacklist_codes.setter
    def blacklist_codes(self, blacklist_codes):
        self._blacklist_codes = blacklist_codes

    def process_blacklist(self):
        self._blacklist_codes = csv.reader(open(self.blacklist_file))

    def is_blacklisted(self, destination):
        """

        :param destination:
        :return:
        """

        for blacklist_code in self.blacklist_codes:
            if len(blacklist_code) == 1:
                if destination.startswith(str(blacklist_code[0])):
                    return True
        return False

from conf import settings
from utils import Generator
from utils import helper


class CampaignD(object):
    """ Campaign object
    """

    def __init__(self):
        """

        :return:
        """

        self.reference = Generator.Generator().generate_job(10)
        self._filename = None
        self._provider = None
        self._test = None
        self._owner_id = None
        self._cps = None
        self._country_code = settings.call_country_code
        self._record = False
        self._transcription = False
        self._blacklist = False
        self._blacklist_file = None
        self._lookup = False
        self._lookup_service = None


    @property
    def lookup_service(self):
        return self._lookup_service

    @lookup_service.setter
    def lookup_service(self, lookup_service):
        self._lookup_service = lookup_service

    @property
    def lookup(self):
        return self._lookup

    @lookup.setter
    def lookup(self, lookup):
        self._lookup = lookup

    @property
    def blacklist_file(self):
        return self._blacklist_file

    @blacklist_file.setter
    def blacklist_file(self, blacklist_file):
        self._blacklist_file = blacklist_file

    @property
    def blacklist(self):
        return self._blacklist

    @blacklist.setter
    def blacklist(self, blacklist):
        self._blacklist = blacklist

    @property
    def cps(self):
        return self._cps

    @cps.setter
    def cps(self, cps):
        self._cps = cps

    @property
    def country_code(self):
        return self._country_code

    @country_code.setter
    def country_code(self,country_code):
        self._country_code = country_code

    @property
    def record(self):
        return self._record

    @record.setter
    def record(self, record):
        self._record = record

    @property
    def transcription(self):
        return self._transcription

    @transcription.setter
    def transcription(self, transcription):
        self._transcription = transcription

    @property
    def filename(self):
        return self._filename

    @filename.setter
    def filename(self, filename):
        self._filename = filename

    @property
    def provider(self):
        return self._provider

    @provider.setter
    def provider(self, provider):
        self._provider = provider

    @property
    def test(self):
        return self._test

    @test.setter
    def test(self, test):
        self._test = test

    def terminate(self):
        if self.reference:
            sqlquery = 'UPDATE campaign SET campaign_end=\'now()\' WHERE campaign.reference=\'' + self.reference + '\''
            helper.update_database(sqlquery)
        
    def __repr__(self):
        return self.reference

    def __str__(self):
        return self.reference

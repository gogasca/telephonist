import logging
import os
import random
import re
import string
import time

import speech_recognition as sr

import notify
from conf import logger
from conf import settings
from error import exceptions

log = logger.LoggerManager().getLogger("__app__")
log.setLevel(level=logging.DEBUG)


class SpeechRecognition(object):
    def __init__(self):
        self._audiofile = None
        self._callsid = None
        self._uuid = None
        self._text = None
        self._analysis = None
        self.country_code = ''

    @property
    def uuid(self):
        return self._uuid

    @uuid.setter
    def uuid(self, uuid):
        self._uuid = uuid

    @property
    def callsid(self):
        return self._callsid

    @callsid.setter
    def callsid(self, callsid):
        self._callsid = callsid

    @property
    def audiofile(self):
        return self._audiofile

    @audiofile.setter
    def audiofile(self, audiofile):
        self._audiofile = audiofile

    @property
    def text(self):
        return self._text

    @text.setter
    def text(self, text):
        self._text = text

    @property
    def analysis(self):
        return self._analysis

    @analysis.setter
    def analysis(self, analysis):
        self._analysis = analysis

    def update_database_record(self, code=1):
        if code == 1:
            if notify.call_recording_text(self.callsid, self.uuid, self.text):
                log.info('SpeechRecognition() Record update: {} {} {}'.format(self.callsid, self.uuid, self.text))
                return True
            else:
                log.error('SpeechRecognition() Database update failed')
                return False

        # Perform text analysis
        if code == 2:
            if notify.call_recording_analysis(self.callsid, self.analysis):
                log.info(self.callsid, self.text)
                return True
            else:
                log.error('SpeechRecognition() Database update failed')
                return False

    @staticmethod
    def normalize(text):
        """

        :param text:
        :return:
        """
        allow = string.letters + string.digits + string.whitespace
        if text:
            text = re.sub('[^%s]' % allow, '', text)
            if text:
                return text.lower()

        return ''

    def audio_to_text(self):
        """

        :return:
        """
        if self.audiofile:
            r = sr.Recognizer()
            with sr.AudioFile(self.audiofile) as source:
                audio = r.record(source)
            try:
                # Get Audio from Speech to Text API
                self.text = r.recognize_sphinx(audio, language=settings.speech_recognition_language)
                log.info('SpeechRecognition could not understand audio')
                self.text = self.normalize(self.text)
                return self.text
            except sr.UnknownValueError:
                log.error("SpeechRecognition could not understand audio")
            except sr.RequestError as e:
                log.error("SpeechRecognition error; {0}".format(e))
        else:
            raise exceptions.LocalFileDoesntExist("No audiofile {}".format(self.audiofile))

    def transcribe_audio_google(self):
        """

        :param
        transcribes audio using google
        """
        audio_path = settings.audio_dir + self.audiofile
        lang = ""
        self.text = "~"

        if audio_path.find('.wav') < 0 or not os.path.exists(audio_path):
            self.text = 'RECORDING_FILE_NOT_FOUND'
            return self.text

        if self.country_code in settings.language_codes.keys():
            lang = settings.language_codes[self.country_code]

        r = sr.Recognizer()

        with sr.AudioFile(audio_path) as source:
            audio = r.record(source)  # read the entire audio file
        for i in range(3):
            try:
                self.text = r.recognize_google(audio, None, lang)
                break
            except sr.UnknownValueError:
                self.text = "TRANSCRIPTION_FAILED"
                time.sleep((2 ** i) + random.random())
            except sr.RequestError as e:
                self.text = "REQUEST_FAILED"
                log.error("Google SR could not understand audio {0}".format(e))
                break
            except UnicodeEncodeError as e:
                self.text = "UNICODE_ERROR"
                log.error("Google SR Error; {0}".format(e))
                break
        return self.text

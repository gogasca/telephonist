#!/bin/bash
BROKER_URL=" --broker=amqp://telephonist:telephonist@rabbitmq:5672//"
USERNAME="root"
PASSWORD="M1Nub3"
PORT=8082
PERSISTENT=" --persistent=True --db=/usr/src/telephonist/log/flower.db"
exec /usr/bin/python /usr/local/bin/flower --basic_auth=$USERNAME:$PASSWORD --port=$PORT $BROKER_URL $PERSISTENT
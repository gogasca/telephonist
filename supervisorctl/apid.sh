#!/bin/bash
cd /usr/src/telephonist/api/version1_0/
GUNICORN_LOGFILE=/usr/src/telephonist/log/gunicorn.log
NUM_WORKERS=4
TIMEOUT=60
WORKER_CONNECTIONS=5000
BACKLOG=2500
exec gunicorn telephonist_api:api_app --bind 0.0.0.0:8081 --log-level=CRITICAL --log-file=$GUNICORN_LOGFILE --workers $NUM_WORKERS --worker-connections=$WORKER_CONNECTIONS --backlog=$BACKLOG --timeout $TIMEOUT
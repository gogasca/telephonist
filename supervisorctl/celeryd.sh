#!/bin/bash
# Parzee Inc 2016
# Gonzalo Gasca Meza gonzalo@parzee.com
# Program: Celery start script for Supervisor
# Declare ENV variables TELEPHONIST_CLIENT and TELEPHONIST_MODE
# Use celeryd.conf|celerydclient.conf to launch this script.
#
source ~/.profile
DEBUG_MODE="INFO"
CELERY_LOGFILE=/usr/src/telephonist/log/%n.log
# If you change CELERY_PROCESSES verify you change CELERYD_PREFETCH_MULTIPLIER in conf/celeryconfig.py
CELERY_PROCESSES=30
CELERY_CLIENT_PROCESSES=5
CELERY_PID_FILE=/usr/src/telephonist/log/%n.pid
CELERYD_CLIENT_OPTS="-P processes -c $CELERY_CLIENT_PROCESSES -Q bronze --loglevel=$DEBUG_MODE --pidfile=$CELERY_PID_FILE"
CELERYD_RECORDING_OPTS="-P processes -c $CELERY_PROCESSES -Q silver --loglevel=$DEBUG_MODE --pidfile=$CELERY_PID_FILE"
CELERYD_SERVER_OPTS="-P processes -c $CELERY_PROCESSES --loglevel=$DEBUG_MODE --pidfile=$CELERY_PID_FILE -Ofair"
cd /usr/src/telephonist/conf
if [ "$2" = "client" ]; then
    echo "Client mode"
    exec celery worker -n celeryd$1@%h -f $CELERY_LOGFILE $CELERYD_CLIENT_OPTS
fi
if [ "$2" = "recording" ]; then
    echo "Record server mode"
    exec celery worker -n celeryd$1@%h -f $CELERY_LOGFILE $CELERYD_RECORDING_OPTS
fi
if [ "$2" = "server" ]; then
    echo "Server mode"
    exec celery worker -n celeryd$1@%h -f $CELERY_LOGFILE $CELERYD_SERVER_OPTS
fi
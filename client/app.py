from tornado import gen, httpclient, ioloop
import json
from utils import Generator, FileReader

http_client = httpclient.AsyncHTTPClient()
api_url = 'http://localhost:8081/api/1.0/call'
DIRECTORY = 'e164.txt'


@gen.coroutine
def post(body):
    try:

        print 'Campaign: {}'.format(body['campaign'])
        print 'Number of Phones: {}'.format(FileReader.mapcount(DIRECTORY))

        with open(DIRECTORY) as phone_numbers:
            for phone_number in phone_numbers:
                body['to'] = phone_number.rstrip()
                headers = {'Content-Type': 'application/json'}
                request = httpclient.HTTPRequest(auth_username='AC64861838b417b555d1c8868705e4453f',
                                                 auth_password='YYPKpbIAYqz90oMN8A11YYPKpbIAYqz90o',
                                                 url=api_url,
                                                 body=json.dumps(body),
                                                 headers=headers,
                                                 request_timeout=15,
                                                 method="POST")
                response = yield http_client.fetch(request)
                print body['to'], response.code

    except httpclient.HTTPError, e:
        print("Error: " + str(e))
    except Exception, e:
        print("Exception: " + str(e))


def start_request():
    """

    :return:
    """
    body = dict()
    body["campaign"] = Generator.Generator().generate_job(8)
    body["from"] = "+16506814252"
    body["url"] = "http://73ee2e07.ngrok.io/api/1.0/callrecord"
    body["status_callback"] = "http://73ee2e07.ngrok.io/api/1.0/callstatus"
    body["duration"] = 18

    ioloop.IOLoop.current().run_sync(lambda: post(body))


start_request()

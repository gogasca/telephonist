from tornado import httpclient, gen, ioloop
import json
import urllib


class NewCall(object):
    """
    """

    def __init__(self):
        self._api_url = None

    @property
    def api_url(self):
        return self._api_url

    @api_url.setter
    def api_url(self, api_url):
        self._api_url = api_url

    def place_call(self, body=None, **kwargs):
        """
        Sends an Call message through the Call gateway to 'number' with the
        specified 'campaign' content.

        :param description:
        :param kwargs:
        :return:
        """
        http_client = httpclient.AsyncHTTPClient()
        headers = {'Content-Type': 'application/json'}
        response = yield gen.Task(http_client.fetch,
                                  url=self.api_url,
                                  method='POST',
                                  headers=headers,
                                  body=body,
                                  auth_username=self._api_account_sid,
                                  auth_password=self._api_token)

        if response.error:
            raise RuntimeError('API error: %d %r [%s] (args: %r)' % (response.code,
                                                                     response.error,
                                                                     response.body, args))
        raise gen.Return(json.loads(response.body))


def process_calls():
    """

    :return:
    """
    body = '{"campaign": "TEST", ' \
                       '"from": "+16506814252", ' \
                       '"to": "+14082186575", ' \
                       '"url": "http://73ee2e07.ngrok.io/api/1.0/callrecord/", ' \
                       '"status_callback": "http://73ee2e07.ngrok.io/api/1.0/callstatus", ' \
                       '"duration": 18 }'

    client_instance = NewCall()
    client_instance.api_account_sid='AC64861838b417b555d1c8868705e4453f'
    client_instance._api_token = 'YYPKpbIAYqz90oMN8A11YYPKpbIAYqz90o'
    client_instance.place_call(body)
    ioloop.IOLoop.instance().start()
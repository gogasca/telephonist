# Telephonist version 1.0
============================

|pypi| |versions| |pypi_downloads|

#About

    API to manage calls to Twilio and transcript after call is completed.
    Infrastructure:
        - 3 API servers
            2 call processors   (4 GB)
            1 recording server  (4 GB)
        
        - 2 RabbitMQ servers    (2 GB)        
        - 1 Load balancer       (2 GB)
        
        - 1 Database server     (4 GB)        
        - 1 Freeswitch          (2 GB)        
        - 1 PSTN Emulator *     (2 GB)
        
        Total servers per deployment: 9 (No HA)
        

#Freeswitch
```
wget -O - https://files.freeswitch.org/repo/deb/debian/freeswitch_archive_g0.pub | apt-key add -
 
echo "deb http://files.freeswitch.org/repo/deb/freeswitch-1.6/ jessie main" > /etc/apt/sources.list.d/freeswitch.list
apt-get update && apt-get install -y freeswitch-meta-all
```

#API server install libraries

    Install Ubuntu 14.04+

```    
    apt-get update
    apt-get install python build-essential python-setuptools swig -y
    apt-get install python-pip python-dev -y
    apt-get install python-numpy python-scipy libatlas-dev -y    
    apt-get install python-sklearn -y    
    apt-get install libpq-dev -y    
    apt-get install git -y
    apt-get flac

    
    cd /usr/src
    git clone https://gogasca@bitbucket.org/gogasca/telephonist.git
    
    Create log directory    
    mkdir /usr/src/telephonist/log
    
    #Tools
    
    apt-get install fail2ban multitail unzip -y
    
    #Python Libraries
    
    pip install --upgrade pip
	pip install -r telephonist/requirements.txt
	mkdir -p /etc/supervisor/conf.d	    
	    cp /usr/local/bin/supervisorctl /usr/bin/
	    cp /usr/local/bin/supervisord /usr/bin/
	       
        Copy conf/supervisord/supervisord.conf to /etc/supervisor/
        Copy the rest of .conf files from conf/supervisord/ folder to /etc/supervisor/conf.d
                	   
	Create directory for log file
	    mkdir /var/log/supervisor/
	
	You may need to upgrade supervisor
    pip install supervisor -U
    
```
http://stackoverflow.com/questions/12900402/supervisor-and-environment-variables/38690097#38690097
```


    pip install coverage
    pip install flower
    pip install nltk
    pip install pandas
    
    vim ~/.bashrc
    vim ~/.profile
    
    Define Environment variables 
    
    export C_FORCE_ROOT="true"    
    export TELEPHONIST_ENV="production"
    export TELEPHONIST_MODE="server"        #Options: server,recording
    export TELEPHONIST_CLIENT="client"      #Options: client
    
    Update variables
    source ~/.bashrc
    source ~/.profile
    
    Create temporary recordings file
    mkdir -p /var/log/telephonist/recordings/
	
     vim /etc/hosts
     Add database, loadbalancer, rabbitmq and recording host entries
     *Production system have a separate database, loadbalancer, rabbitmq & recording system.
		
	* Install Dependencies for API (Check section below)
	
	Start supervisor after reboot:
       supervisord -c supervisord.conf
       
       Use supervisorctl to check services status
       
       
```

#Dependencies for API (Freeswitch ESL library)
```
    
    cd /usr/src/
    git clone git://github.com/sangoma/switchy.git
    pip install -r switchy/requirements-doc.txt switchy/
    
    https://freeswitch.org/confluence/display/FREESWITCH/Ubuntu+14.04+Trusty
    https://github.com/sangoma/switchy
      
```

Celery
------------------------------------------------------------------------------

cd /usr/src/telephonist/conf/
celery purge



Freeswitch - Lighttpd
------------------------------------------------------------------------------

 - Install Lighttpd in Freeswitch server
 - Lighttpd acts as a web server to handle Recordings
 - Configure: /etc/lighttpd/lighttpd.conf
 - Change port
 - Change user directory /var/www/html/recordings
 - Create /var/www/html/recordings
 - Configure /var/www/html/recordings
 - Permissions 750
 - server.username             = "freeswitch"
   server.groupname            = "freeswitch"
   
   chown freeswitch /var/log/lighttpd
   chgrp freeswitch /var/log/lighttpd 
 
```
 chmod 750 /var/www/html/recordings
 chown freeswitch user
 chgrp freeswitch group

```


Database
------------------------------------------------------------------------------
Configure PostgreSQL 9.5

```
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" >> /etc/apt/sources.list.d/pgdg.list'
wget -q https://www.postgresql.org/media/keys/ACCC4CF8.asc -O - | sudo apt-key add -
sudo apt-get update
sudo apt-get install postgresql postgresql-contrib
```

Enable all interfaces to listen on port 5432

```

/etc/postgresql/9.5/main/postgresql.conf
listen_addresses = '*'          # what IP address(es) to listen on;

max_connections = 1024
# - Memory -

shared_buffers = 512MB                  # min 128kB
temp_buffers = 8MB                      # min 800kB
work_mem = 1MB                          # min 64kB
```

Add remote IP addresses connecting to DB:
```
 /etc/postgresql/9.5/main/pg_hba.conf

```

Export database
------------------------------------------------------------------------------    
    pg_dump -s telephonistdb > telephonistdb.db
------------------------------------------------------------------------------

Import database
--------
Default password for user postgres is "postgres"
For user imbue you can use password as "telephonist"


```
    sudo -i -u postgres
    createuser --interactive
    Enter name of role to add: telephonist
    Shall the new role be a superuser? (y/n) n
    Shall the new role be allowed to create databases? (y/n) n
    Shall the new role be allowed to create more new roles? (y/n) n


    psql -c '\l'
    psql -d template1
    template1=> CREATE DATABASE telephonistdb WITH OWNER telephonist ENCODING 'UTF8';
    CREATE DATABASE
    template1=> grant all privileges on database telephonistdb to telephonist;
    template1=> ALTER USER "telephonist" WITH PASSWORD 'telephonist';
    template1=>\q

    psql telephonistdb < telephonistdb.db

    TRUNCATE TABLE calls RESTART IDENTITY;
```

#HAProxy
```
apt-get install socat
#!shell
Clean stats
echo "clear counters all" | socat stdio /etc/haproxy/haproxysock
```

#RabbitMQ

```
#!shell

echo 'deb http://www.rabbitmq.com/debian/ testing main' | sudo tee /etc/apt/sources.list.d/rabbitmq.list
wget -O- https://www.rabbitmq.com/rabbitmq-release-signing-key.asc | sudo apt-key add -

apt-get update
apt-get install rabbitmq-server -y

rabbitmqctl add_user telephonist telephonist
rabbitmqctl set_user_tags telephonist administrator
rabbitmqctl set_permissions -p / telephonist ".*" ".*" ".*"

rabbitmqctl status | grep -A 4 file_descriptors

add following line in /etc/security/limits.conf

* soft nofile 65000
* hard nofile 65000

Edit /etc/default/rabbitmq-server

ulimit -n 65536

rabbitmq-plugins enable rabbitmq_management

Edit /etc/rabbitmq/rabbitmq.config

[{rabbit, [{vm_memory_high_watermark, 0.6}]}].

# Reset factory settings
rabbitmqctl stop_app
rabbitmqctl reset    # Be sure you really want to do this!
rabbitmqctl start_app

celery amqp queue.delete silver

http://hustoknow.blogspot.com/2011/08/what-prefetch-in-celery-means.html
https://bugs.launchpad.net/openquake-old/+bug/1092050

```

#Freeswitch

```
#!shell
sudo apt-get install sip-tester
apt-get install python-pip
apt-get install python-dev
apt-get install tcpdump
apt-get build-dep python-psycopg2
pip install psycopg2


autoload_configs/acl.conf.xml 

    <list name="loopback.auto" default="allow">
        <node type="allow" cidr="172.31.0.0/16"/>
        <node type="allow" cidr="52.67.85.153/32"/>
	<node type="allow" cidr="52.67.97.171/32"/>
	<node type="allow" cidr="52.67.33.247/32"/>	
    </list>
    
sip_profiles/external.xml
sip_profiles/internal.xml
vim autoload_configs/modules.conf.xml
/etc/freeswitch/dialplan/default.xml

/usr/share/freeswitch/scripts/

>load mod_python 

freeswitch -ncwait -nonat

Debugging levels:
http://freeswitch-users.2379917.n2.nabble.com/SIP-Trace-td6382345.html

fsctl loglevel 3
sofia tracelevel 3
sofia profile external siptrace on

https://makandracards.com/makandra/36727-get-haproxy-stats-informations-via-socat

UPDATE calls set hangup_cause_override=hangup_cause WHERE calls.campaign='MAEEKK0IBO';
802018b3-d81e-470e-bd4a-96e07b40c6ba

http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-using-volumes.html

/dev/xvdf /var/www/html/recordings ext4 defaults,nofail        0       2
UUID=802018b3-d81e-470e-bd4a-96e07b40c6ba /var/www/html/recordings ext4 defaults,nofail        0       2
df -h .
find . -name '*.wav' -exec rm {} +
for f in *.pdf; do rm "$f"; done

    <!-- Mexico -->
     <descriptor name="52">
       <tone name="CED_TONE">
         <element freq1="2100" freq2="0" min="700" max="0"/>
       </tone>
       <tone name="RING_TONE">
         <element freq1="425" freq2="0" min="970" max="4016"/>
       </tone>
       <tone name="REORDER_TONE">
         <element freq1="425" freq2="0" min="32" max="96"/>
      	 <element freq1="0" freq2="0" min="32" max="96"/> 
	</tone>
     </descriptor>

    <!-- Brazil -->
     <descriptor name="55">
       <tone name="CED_TONE">
         <element freq1="2100" freq2="0" min="700" max="0"/>
       </tone>
       <tone name="RING_TONE">
         <element freq1="425" freq2="0" min="1000" max="1140"/>
       </tone>
       <tone name="REORDER_TONE">
         <element freq1="425" freq2="0" min="256" max="272"/>
         <element freq1="0" freq2="0" min="224" max="240"/>
         <element freq1="425" freq2="0" min="720" max="784"/>
         <element freq1="0" freq2="0" min="240" max="256"/>
       </tone> 
       <tone name="BUSY_TONE">
         <element freq1="425" freq2="0" min="256" max="272"/>
         <element freq1="0" freq2="0" min="240" max="256"/>  
         <element freq1="425" freq2="0" min="256" max="272"/>  
         <element freq1="0" freq2="0" min="224" max="240"/>           
       </tone>
     </descriptor>
     
     BUSY
     2016-08-21 09:27:37.762531 [DEBUG] mod_spandsp_dsp.c:705 Tone segment: f1 = -1, f2 = -1, duration = 256
     2016-08-21 09:27:38.002616 [DEBUG] mod_spandsp_dsp.c:705 Tone segment: f1 = 1, f2 = -1, duration = 272
     2016-08-21 09:27:38.262532 [DEBUG] mod_spandsp_dsp.c:705 Tone segment: f1 = -1, f2 = -1, duration = 240
     2016-08-21 09:27:38.542533 [DEBUG] mod_spandsp_dsp.c:705 Tone segment: f1 = 1, f2 = -1, duration = 272
     
     REORDER
     2016-08-21 09:17:28.802524 [DEBUG] mod_spandsp_dsp.c:705 Tone segment: f1 = 1, f2 = -1, duration = 272
     2016-08-21 09:17:29.042598 [DEBUG] mod_spandsp_dsp.c:705 Tone segment: f1 = -1, f2 = -1, duration = 240
     2016-08-21 09:17:29.802562 [DEBUG] mod_spandsp_dsp.c:705 Tone segment: f1 = 1, f2 = -1, duration = 768
     2016-08-21 09:17:30.062525 [DEBUG] mod_spandsp_dsp.c:705 Tone segment: f1 = -1, f2 = -1, duration = 256

    Test cases
    ----------
    
    Completed:
    
    1. Call connects: Remote end disconnect (200 OK) Normal_Clearing (+55-1940422255)
    2. Call connects: We disconnect (200 OK) Alloted_Timeout (+55-2140422882)
    3. Fax - Normal_Clearing (+55-1132844424)
    
    Failed/Error condition:
    
    4. SIP returns error code > 400 - Inherit error (+55-1124233430)
    5. Call 183/SDP ringing ring tone - No answer. (+55-5353933443)
    6. Call 183/SDP ringing busy tone. - Busy (+55-6137048640)
    7. Call 183/SDP ringing reorder tone - Invalid_destination -
    8. Call 183/SDP ringing no tone - Announcement (+55-11995561759)
    9. Call connects: Tone is detected (Busy) - Busy (call length less than 6 secs) - Can be emulated with +55-2140422882 pointing to Asterisk busy
    10. Call connects: Tone is detected (Reorder) - Invalid_destination (call length less than 6 secs)


```

#API

API request example:
```
#!shell

curl -u AC64861838b417b555d1c8868705e4453f:YYPKpbIAYqz90oMN8A11YYPKpbIAYqz90o -H "Content-Type: application/json" -XPOST -d '{"filename": "/tmp/test5.txt", "provider": "sip", "test": true }' http://192.241.198.110:8080/api/1.0/campaign
curl -u AC64861838b417b555d1c8868705e4453f:YYPKpbIAYqz90oMN8A11YYPKpbIAYqz90o -H "Content-Type: application/json" -X  POST -d '{"campaign": "TEST", "from": "+16506814252", "to": "4082186575", "url": "http://73ee2e07.ngrok.io/api/1.0/callrecord", "status_callback": "http://73ee2e07.ngrok.io/api/1.0/callstatus", "duration": 15 }' http://0.0.0.0:8081/api/1.0/call
curl -u AC64861838b417b555d1c8868705e4453f:YYPKpbIAYqz90oMN8A11YYPKpbIAYqz90o -H "Content-Type: application/json" -X  POST -d '{"to": "4085680004", "from": "+18623079305", "description": "", "campaign": "MXIE38SQ7I", "provider": "sip", "test": true, "duration": 18}' http://0.0.0.0:8081/api/1.0/call

Create a new file, curl-format.txt, and paste in:

    time_namelookup:  %{time_namelookup}\n
       time_connect:  %{time_connect}\n
    time_appconnect:  %{time_appconnect}\n
   time_pretransfer:  %{time_pretransfer}\n
      time_redirect:  %{time_redirect}\n
 time_starttransfer:  %{time_starttransfer}\n
                    ----------\n
         time_total:  %{time_total}\n

Make a request:

curl -w "@curl-format.txt" -o /dev/null -s -u AC64861838b417b555d1c8868705e4453f:YYPKpbIAYqz90oMN8A11YYPKpbIAYqz90o -H "Content-Type: application/json" -X  POST -d '{"to": "4085680004", "from": "+18623079305", "description": "", "campaign": "MXIE38SQ7I", "provider": "sip", "test": true, "duration": 18}' http://159.203.222.147:8081/api/1.0/call

```


```
#!undefined

<!--
    <extension name="telephonist">
         <condition field="destination_number" expression="^handle_calls$">
                <action application="log" data="INFO handle_calls ${uuid} CALL_STARTED"/>
                <action application="set_zombie_exec"/>
                <action application="bridge"/>  
                <action application="spandsp_start_fax_detect" data="transfer 'fax' 10 ced"/>
                <action application="spandsp_start_tone_detect" data="55"/>
                <action application="playback" data="silence_stream://32000"/>
                <action application="log" data="WARNING SIP: ${sip_reason} SHD: ${sip_hangup_disposition} LBP: ${last_bridge_proto_specific_hangup_cause} HC : ${hangup_cause} / BHC: ${bridge_hangup_cause} / OD: ${originate_disposition}" />
                <action application="set" data="sip_bye_h_X-Bye-Reason=${sip_reason}"/>
                <action application="set" data="sip_bye_h_X-Remote-Reason=${bridge_hangup_cause}" />
                <action application="spandsp_stop_tone_detect"/>
                <action application="log" data="INFO handle_calls ${uuid} CALL_COMPLETED"/>
        </condition>
   </extension>
-->
```


#Swap memory

You can first check if there is any swap memory enabled.


```
#!undefined

sudo swapon -s
if it is empty, it means you don't have any swap enabled. To add a 1GB swap:

sudo dd if=/dev/zero of=/swapfile bs=1024 count=1024k
sudo mkswap /swapfile

sudo swapon /swapfile
Add the following line to the fstab to make the swap permanent.

sudo vim /etc/fstab
```


     /swapfile       none    swap    sw      0       0

#Unmount volume

http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-detaching-volume.html

#Mount volume

http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-using-volumes.html


```
#!undefined

ulimit -c unlimited # The maximum size of core files created.
ulimit -d unlimited # The maximum size of a process's data segment.
ulimit -f unlimited # The maximum size of files created by the shell (default option)
ulimit -i unlimited # The maximum number of pending signals
ulimit -n 9999999    # The maximum number of open file descriptors.
ulimit -q unlimited # The maximum POSIX message queue size
ulimit -u unlimited # The maximum number of processes available to a single user.
ulimit -v unlimited # The maximum amount of virtual memory available to the process.
ulimit -x unlimited # ???
ulimit -l unlimited # The maximum size that may be locked into memory.
ulimit -a           # All current limits are reported.
```